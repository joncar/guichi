<?php

class Contactos  extends grocery_CRUD_Model  {
    
    function __construct()
    {
        parent::__construct();
    }   

    function get_list()
    {
        $this->db->select('user.id,user.foto,user.nombre as name,user.apellido,user.sexo,user.email,user.ciudad,user.direccion,user.movil,user.empresa as emp,user.puesto');
        $this->db->where('contactos.user',$_SESSION['user']);
        $this->db->join('contactos','contactos.contacto = user.id','right');
        $result = $this->db->get('user');
    	return $result->result();
    }    

}
