<?php 

class Mails extends CI_Model{
  
    function send_invitaciones($id_reunion){
        //Enviar mails
        $reunion = $this->db->get_where('reuniones',array('id'=>$id_reunion,'status !='=>2));
        $invitados = $this->db->get_where('invitados',array('reunion'=>$id_reunion));
        $temas = $this->db->get_where('temas',array('reunion'=>$id_reunion));
        
        $this->db->select('user.*');
        $this->db->join('user','user.id = reuniones.user');
        $this->db->join('invitados','invitados.reunion = reuniones.id');
        $lider = $this->db->get_where('reuniones',array('reuniones.id'=>$id_reunion,'lider'=>1))->row();
        if($reunion->num_rows>0){        
        $reunion = $reunion->row();
        foreach($invitados->result() as $t){
            $user = $this->db->get_where('user',array('id'=>$t->invitado))->row();
            $mail = '';
            if($user->password==md5('12345678')){
                $mail .= '
                    <div style="margin:30px;">
                        Hola: <b>'.$user->nombre.' '.$user->apellido.' ('.$user->email.')</b><br/>
                        Usuario: '.$user->email.'<br/>
                        Password: 12345678<br/>
                    </div>
                ';                    
            }

            $mail.= '
                <div style="margin:30px">
                    <b>'.$lider->nombre.' '.$lider->apellido.'</b> te ha invitado a una reunión de trabajo: <br/>
                    <b>Título: </b> '.$reunion->titulo.' <br/><ul>';
            
            foreach($temas->result() as $n=>$t){
                $mail.= '<li>Tema '.($n+1).'. '.$t->tema.'</li>';
            }
            $mail.= '</ul><br/><b>Nota: </b>'.$reunion->nota.'.<br/>
                    -<b> Cuándo: </b>'.date("d/m/Y",strtotime($reunion->fecha)).' - '.$reunion->hora.' Hrs <br/>
                    -<b> Dónde: </b>'.$reunion->lugar.' <br/>
                    -<b> Invitados: </b><br/><ul>';
            
            foreach($invitados->result() as $t){
                $invitado = $this->db->get_where('user',array('id'=>$t->invitado))->row();
                 $mail.= '<li> '.$invitado->nombre.' '.$invitado->apellido.' </li>';
            }    

            $mail.= '</ul><br/>
                - Asistirás? <a href="'.base_url('asist/'.$id_reunion).'">Si</a>  -  <a href="'.base_url('nasist/'.$id_reunion).'">No</a> (Podría con un clic aquí el usuario confirmar su asistencia?) <br/>
                - Para ingresar a la reunión y ver más detalles por favor entra a <a href="'.site_url().'">www.guichi.com.mx</a> <br/>
                </div>
                <div style="border-top:1px dashed lightgray; color:gray; padding:20px; font-size:12px;">
                Este mensaje es un recordatorio del Portal Guichi.com.mx en el que '.$lider->nombre.' '.$lider->apellido.' te ha registrado, si deseas ser dado de baja, por favor envía un mail con el título BAJA al correo electrónico soporte@guichi.com.mx, con esta petición tenemos 30 días naturales como máximo para darte de baja de nuestro portal.
                </div>
            ';

            correo($user->email,'Hola '.$user->nombre.' te han invitado a una Reunión a través de  Guichi.com.mx',$mail);
            correo('joncar.c@gmail.com','Hola '.$user->nombre.' te han invitado a una Reunión a través de  Guichi.com.mx',$mail);
        }      
        }
    }
    
    function send_cancelacion($id_reunion){
        //Enviar mails
        $reunion = $this->db->get_where('reuniones',array('id'=>$id_reunion));
        $invitados = $this->db->get_where('invitados',array('reunion'=>$id_reunion));
        $temas = $this->db->get_where('temas',array('reunion'=>$id_reunion));
        
        $this->db->select('user.*');
        $this->db->join('user','user.id = reuniones.user');
        $this->db->join('invitados','invitados.reunion = reuniones.id');
        $lider = $this->db->get_where('reuniones',array('reuniones.id'=>$id_reunion,'lider'=>1))->row();
        if($reunion->num_rows>0){
        $reunion = $reunion->row();
        foreach($invitados->result() as $t){
            $user = $this->db->get_where('user',array('id'=>$t->invitado))->row();
            $mail = '';            
            $mail.= '
                <div style="margin:30px">
                    <b>'.$lider->nombre.' '.$lider->apellido.'</b> se ha cancelado la reunión: <br/>
                    <b>Título: </b> '.$reunion->titulo.' <br/><ul>';
            
            foreach($temas->result() as $n=>$t){
                $mail.= '<li>Tema '.($n+1).'. '.$t->tema.'</li>';
            }
            $mail.= '</ul><br/><b>Nota: </b>'.$reunion->nota.'.<br/>
                    -<b> Cuándo: </b>'.date("d/m/Y",strtotime($reunion->fecha)).' - '.$reunion->hora.' Hrs <br/>
                    -<b> Dónde: </b>'.$reunion->lugar.' <br/>
                    -<b> Invitados: </b><br/><ul>';
            
            foreach($invitados->result() as $t){
                $invitado = $this->db->get_where('user',array('id'=>$t->invitado))->row();
                 $mail.= '<li> '.$invitado->nombre.' '.$invitado->apellido.' </li>';
            }    

            $mail.= '</ul><br/>                
            ';

            correo($user->email,'Hola '.$user->nombre.' Se ha cancelado una reunión de Guichi.com.mx',$mail);
            correo('joncar.c@gmail.com','Hola '.$user->nombre.' Se ha cancelado una reunión de Guichi.com.mx',$mail);
        }      
        }
    }
    
    function send_reuniones(){
        $this->db->where('status',0);
        $this->db->where('UNIX_TIMESTAMP(recordatorio) != UNIX_TIMESTAMP(\'0000-00-00\')',null,FALSE);
        $this->db->where('UNIX_TIMESTAMP(recordatorio) <= UNIX_TIMESTAMP(\''.date("Y-m-d H:i:s").'\')',null,FALSE);
        $reuniones = $this->db->get('reuniones');        
        foreach($reuniones->result() as $r){
            $this->send_invitaciones($r->id);
            
            $siguiente = date("Y-m-d H:i:s",strtotime('-1 hours '.$r->fecha.' '.$r->hora));
            if($siguiente==$r->recordatorio && $r->recordatorio!='0000-00-00 00:00:00')
                $this->db->update('reuniones',array('recordatorio'=>'0000-00-00'),array('id'=>$r->id));
            else
            $this->db->update('reuniones',array('recordatorio'=>$siguiente),array('id'=>$r->id));
        }
    }
    
    function send_tarea(){
        //Enviar mails      
        $this->db->join('reuniones','tareas.reunion = reuniones.id');
        $this->db->select('reuniones.*, tareas.*, tareas.id as idtarea, tareas.recordatorio as tarearecord');
        $tareas = $this->db->get_where('tareas',array('reuniones.status'=>'2','progreso < '=>100,'tareas.recordatorio <= '=>date("Y-m-d"),'entrega != '=>'0000-00-00','tareas.recordatorio !='=>'0000-00-00'));
        foreach($tareas->result() as $t){            
            //Si aun no se ha vencido el periodo
            $siguiente = date("Y-m-d",strtotime("+15 days ".$t->tarearecord));
            if((strtotime(date("Y-m-d"))-strtotime($t->entrega))>0)
                $siguiente = date("Y-m-d",strtotime("+2 days ".$t->tarearecord));
            $this->db->update('tareas',array('recordatorio'=>$siguiente),array('id'=>$t->idtarea));
            
            $reunion = $this->db->get_where('reuniones',array('id'=>$t->reunion))->row();
            $user = $this->db->get_where('user',array('id'=>$t->responsable));
            if($user->num_rows>0){
                $user = $user->row();
                $mail = '';
                if($user->password==md5('12345678')){
                    $mail .= '
                        <div style="margin:30px;">
                            Hola: <b>'.$user->nombre.' '.$user->apellido.' ('.$user->email.')</b><br/>
                            Usuario: '.$user->email.'<br/>
                            Password: 12345678<br/>
                        </div>
                    ';                    
                }
                
                $adjunto = !empty($t->adjunto)?1:0;
                $mail .= '            
                    Te recordamos que se te ha encomendado una tarea importante en tu última reunión: 
                    <ul>
                        <li>Título de la Reunión: '.$reunion->titulo.'</li>
                        <li>Tema: <b>'.$this->db->get_where('temas',array('id'=>$t->tema))->row()->tema.'</b></li>
                        <li>Tarea: '.$t->nombre.'</li>
                        <li>Fecha de Entrega: '.date("d/m/Y",strtotime($t->entrega)).'</li>
                        <li>'.$adjunto.' Archivo Adjunto</li>
                        <li>Porcentaje de Avance: '.$t->progreso.'%</li>
                    </ul>
                    Para ingresar a la reunión y ver más detalles por favor entra a <a href="'.site_url().'">www.guichi.com.mx</a>

                    <div style="border-top:1px dashed lightgray; color:gray; padding:20px; font-size:12px;">
                        Este mensaje es un recordatorio del Portal Guichi.com.mx en el que '.$user->nombre.' '.$user->apellido.' te ha registrado, si deseas ser dado de baja, por favor envía un mail con el título BAJA al correo electrónico soporte@guichi.com.mx, con esta petición tenemos 30 días naturales como máximo para darte de baja de nuestro portal.
                    </div>
                ';
                correo($user->email,'Hola '.$user->nombre.' te recordamos que tienes una tarea pendiente a través de Guichi.com.mx',$mail);           
                correo('joncar.c@gmail.com','Hola '.$user->nombre.' te recordamos que tienes una tarea pendiente a través de Guichi.com.mx',$mail);           
            }
        }
    }
    
    function sendperfil($user,$invitador){        
        //Enviar mails        
        $user = $this->db->get_where('user',array('id'=>$user))->row();
        $invitador = $this->db->get_where('user',array('id'=>$invitador))->row();
        $mail = '';
        if($user->password==md5('12345678')){
            $mail .= '
                <div style="margin:30px;">
                    Hola: <b>'.$user->nombre.' '.$user->apellido.' ('.$user->email.')</b> han registrado tus datos en guichi.com.mx, nos gustaria darte la bienvenida e invitarte a que completes tus datos entrando al portal de <a href="'.site_url().'">Guichi</a> Con los siguientes datos:<br/>
                    Usuario: '.$user->email.'<br/>
                    Password: 12345678<br/>
                </div>
            ';                    
        }

        $mail.= '';
        $mail.='
        <div style="border-top:1px dashed lightgray; color:gray; padding:20px; font-size:12px;">
        Este mensaje es un recordatorio del Portal Guichi.com.mx en el que '.$invitador->nombre.' '.$invitador->apellido.' te ha registrado, si deseas ser dado de baja, por favor envía un mail con el título BAJA al correo electrónico soporte@guichi.com.mx, con esta petición tenemos 30 días naturales como máximo para darte de baja de nuestro portal.
        </div>';    
        
        correo($user->email,'Hola '.$user->nombre.' Han registrado tus datos en Guichi.com.mx');
    }
}
?>