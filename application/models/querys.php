<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Querys extends CI_Model{
    var $iniciales = array();
    function __construct()
    {
        parent::__construct();        
    }
    
    function get_foto()
    {
        return empty($_SESSION['foto'])?'img/vacio.png':'files/'.$_SESSION['foto'];
    }
    
    function get_iniciales($user){
        $inicial = substr($user->nombre,0,1).substr($user->apellido,0,1);
        $x = 0;
        $encontrado = 0;
        foreach($this->iniciales as $i)
        {
            if($i[0]==$inicial){
                $x++;
            }
            if($i[1]==$user->id){
                $encontrado = $x;
            }
        }        
        if($encontrado==0){
            array_push($this->iniciales,array($inicial,$user->id));        
        }
        if($x>0 && $encontrado==0){
            $inicial = $inicial.$x;
        }
        if($encontrado>0){
            $inicial = $inicial.$encontrado;
        }
        return $inicial;
    }
    
    function setIniciales($contactos){
        foreach($contactos->result() as $n=>$i)
        {
            $inicial = strtoupper(substr($i->nombre,0,1).substr($i->apellido,0,1));
            if(in_array($inicial,$this->iniciales)){
                $x = 0;
                foreach($this->iniciales as $i){
                    if($i==$inicial){
                        $x++;
                    }
                }
                $this->iniciales[] = $inicial.$x;
                $contactos->row($n)->iniciales = $inicial.$x;
            }else{
                $this->iniciales[] = $inicial;
                $contactos->row($n)->iniciales = $inicial;
            }
        }
        return $contactos;
    }
    
    function get_membresia(){
        
        $this->db->where('user',$_SESSION['user']);
        $this->db->where('Month(fecha)',date("m"));
        $this->db->where('Year(fecha)',date("Y"));
        $pagos = $this->db->get('depositos');
        $_SESSION['premium'] = $pagos->num_rows>0?1:0;
        return $pagos->num_rows>0?'PREMIUM':'FREE';
    }
}
?>
