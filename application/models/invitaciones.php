<?php

class Invitaciones  extends grocery_CRUD_Model  {
    
    function __construct()
    {
        parent::__construct();
    }   

    function get_list()
    {
        $this->db->order_by('fecha','ASC');
        $this->db->join('invitados','invitados.reunion = reuniones.id');                        
        $this->db->where('invitados.invitado',$_SESSION['user']);
        $this->db->select('reuniones.id, reuniones.fecha,reuniones.hora,reuniones.libreta,reuniones.titulo,invitados.convoco,reuniones.avance,reuniones.status',FALSE);
        $result = $this->db->get('reuniones');
    	return $result->result();
    }    

}
