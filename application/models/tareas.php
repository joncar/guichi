<?php

class Tareas  extends grocery_CRUD_Model  {
    
    function __construct()
    {
        parent::__construct();
    }   

    function get_list()
    {
        $this->db->select('tareas.*, tareas.nombre as tarea, temas.tema as tem, concat(user.nombre," ",user.apellido) as resp',FALSE);
        $this->db->join('invitados','invitados.reunion = reuniones.id');
        $this->db->join('tareas','tareas.reunion = reuniones.id');
        $this->db->join('user','user.id = tareas.responsable');
        $this->db->join('temas','temas.id = tareas.tema');
        $this->db->where('invitados.invitado',$_SESSION['user']);
        $this->db->group_by('tareas.id');
        $result = $this->db->get('reuniones');
    	return $result->result();
    }    

}
