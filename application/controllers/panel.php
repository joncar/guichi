<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('main.php');
class Panel extends Main {
        
	public function __construct()
	{
		parent::__construct();
                if(empty($_SESSION['user']))
                header("Location:".base_url('index.php?redirect='.str_replace("/sgv/","",$_SERVER['REQUEST_URI'])));
                $this->load->library('html2pdf/html2pdf');
                $this->load->library('image_crud');                
	}
       
        public function index($url = 'main',$page = 0)
	{		
                $this->loadView('panel');
	}                
        
        public function loadView($crud)
        {
            if(empty($_SESSION['user']))
            header("Location:".base_url('index.php?redirect='.str_replace("/sgv/","",$_SERVER['REQUEST_URI'])));
            else
            parent::loadView($crud);
        }
        
        function usuarios($x = '', $y = '')
        {
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap2');
            $crud->set_table('user');
            $crud->set_subject('Usuarios');               
            //Fields            
            //unsets
            $crud->unset_delete()                                  
                 ->unset_fields()
                 ->unset_columns('password');
            //Displays
            $crud->display_as('password','Contraseña');            
            //Fields types            
            //Validations  
            $crud->required_fields(
                    'nombre','apellido','email',
                    'password','password2',
                    'pais','ciudad','tipodocumento',
                    'lugarnac','sexo','nacionalidad',
                    'acceso_mayorista',
                    'direccion','nrocelular','password','usuario','cedula','rut','admin','status');  
            if(empty($y))
            $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');
            $crud->set_rules('usuario','Usuario','required|is_unique[user.usuario]');
            $crud->set_rules('password','Contraseña','required|min_length[8]');                                
            $crud->field_type('status','true_false',array('0'=>'Bloqueado','1'=>'Activo'));
            $crud->field_type('admin','true_false',array('0'=>'No','1'=>'Si'));
            $crud->field_type('acceso_mayorista','true_false',array('0'=>'No','1'=>'Si'));
            //Callbacks            
            $crud->callback_before_insert(array($this,'usuario_binsertion'));
            $crud->callback_after_insert(array($this,'usuario_ainsertion'));
            $crud->callback_before_update(array($this,'usuario_binsertion'));
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de clientes';
            $output->menu = 'usuarios';
            $this->loadView($output);
        }           
        
        function reuniones(){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('datatables');
            $crud->set_table('reuniones');
            $crud->set_subject('Reunion');  
            $crud->callback_column('libreta',function($val,$row){return $this->db->get_where('empresas',array('id'=>$val))->row()->nombre;});
            $crud->callback_column('avance',function($val,$row){return $val.'%';});
            $crud->callback_column('user',function($val,$row){
                get_instance()->db->join('user','user.id = invitados.invitado');
                $convoco = get_instance()->db->get_where('invitados',array('reunion'=>$row->id,'convoco'=>1));
                if($convoco->num_rows>0){
                    $convoco = $convoco->row();
                    return $convoco->nombre.' '.$convoco->apellido;
                }                
                return 'Sin Invitados';
            });
            $crud->field_type('status','dropdown',array(0=>'<span class="label label-warning">Programada</span>','-1'=>'<span class="label label-danger">Cancelada</span> ','1'=>'<span class="label label-info">En progreso</span>','2'=>'<span class="label label-success">Realizada</span>'));            
            //$crud->set_relation('user','user','{nombre} {apellido}');
            $crud->add_action('<i class="fa fa-book"></i><br/>Abrir reunión','',base_url('reunion').'/','reunion');            
            $crud->display_as('user','Convocó')
                 ->display_as('titulo','Título')
                 ->display_as('id','#nro. reunion');
            $crud->order_by('id','desc');
            $crud->unset_add()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_edit()
                 ->unset_export()
                 ->unset_print()
                 ->columns('id','fecha','hora','libreta','titulo','user','avance','status');  
            return $crud;
        }
                
        function usuario_binsertion($post,$primary = '')
        {                        
            if(!empty($primary) && $this->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'] || empty($primary))
                $post['password'] = md5($post['password']);
            return $post;
        }   
        
        protected function get_reportes($crud)
        {                    
            $this->db->where('controlador',$this->router->fetch_method());                       
            $this->db->select('reportes.*');
            foreach($this->db->get('reportes')->result() as $r){
                if($r->icono!='')
                $crud->add_action('<i title="'.$r->nombre.'" class="fa fa-'.$r->icono.'"></i> <br/>'.$r->nombre,'',base_url('panel/imprimir_reporte/'.$r->id).'/');
                else
                $crud->add_action($r->nombre,'',base_url('panel/imprimir_reporte/'.$r->id).'/');
            }
            return $crud;
        }
        
        public function imprimir_reporte($id,$idtable = '',$return = FALSE,$invitado = '')
        {
            $idtable = empty($idtable)?$id:$idtable;
            $reporte = $this->db->get_where('reportes',array('id'=>$id));
            if($reporte->num_rows>0)
            {
                //Es un reporte con variables? Pues se recogen antes de procesar                
                $variables = $this->db->get_where('reportes_variables',array('reporte'=>$id));
                if($variables->num_rows>0 && empty($_POST))
                {
                    $output = '<form action="'.base_url('panel/imprimir_reporte/'.$id.'/'.$idtable).'" method="post" class="form-group" role="form" onsubmit="return validar(this)">';
                    $output.= '<h3>Por favor complete los datos para generar el reporte</h3>';
                    //Cargar librerias
                    $date = 0;
                    foreach($variables->result() as $v)
                    {
                        switch($v->tipo_dato)
                        {
                            case 'date': $date = 1; break;                            
                        }
                    }
                    
                    if($date == 1)
                     $output.= $this->load->view('predesign/datepicker',null,TRUE);                    
                    //dibujar forms
                    foreach($variables->result() as $v){
                        switch($v->tipo_dato)
                        {
                            case 'date': $class = 'datetime-input'; break;
                        }
                        $output.= '<div class="form-group">'.form_input($v->variable,'','id="field-'.$v->variable.'" class="form-control '.$class.'" placeholder="'.ucfirst($v->variable).'"').'</div>';
                    }
                    $output.= '<div align="center"><button type="submit" class="btn btn-success">Procesar</button></div>';
                    $output.= '</form>';
                    $this->loadView(array('crud'=>'user','view'=>'panel','output'=>$output));
                }
                else{
                $reporte = $reporte->row();
                $texto = base64_decode($reporte->contenido);
                $texto = str_replace('$_USER',$_SESSION['user'],$texto);
                $texto = str_replace('$_ID',$idtable,$texto);
                //Reemplazar variables recogidas en el formulario
                if($variables->num_rows>0)
                {
                    foreach($variables->result() as $v)
                    {
                        if(!empty($_POST[$v->variable]))
                        {
                            if($v->tipo_dato=='date')$_POST[$v->variable] = date("Y-m-d",strtotime(str_replace("/","-",$_POST[$v->variable])));
                            $texto = str_replace('$_'.$v->variable,$_POST[$v->variable],$texto);
                        }
                    }
                }
                $texto = str_replace("&gt;",'>',$texto);
                $texto = str_replace("&lt;",'<',$texto);
                $sql = fragmentar($texto,'{sql="','}');
                
                foreach($sql as $s)
                {
                    list($sentencia,$type) = explode(";",$s);
                    $sentencia = str_replace('"','',$sentencia);                                        
                    switch($type)
                    {
                        case 'data':$texto = str_replace('{sql="'.$s.'}',sqltodata($this->db->query(strip_tags($sentencia))),$texto);
                        case 'table':$texto = str_replace('{sql="'.$s.'}',sqltotable($this->db->query(strip_tags($sentencia))),$texto);
                    }
                }
                $papel = $reporte->papel=='otro'?array($reporte->ancho,$reporte->alto):$reporte->papel;
                
                $func = fragmentar($texto,'{function="','"}');                
                foreach($func as $f){
                    if(!$return)
                    $texto = call_user_func('self::'.$f,$idtable,$f,$texto,$_SESSION['user']);
                    else
                    $texto = call_user_func('self::'.$f,$idtable,$f,$texto,0);
                }
                $html2pdf = new HTML2PDF($reporte->orientacion,$papel,'fr', false, 'ISO-8859-15', array($reporte->margen_izquierdo, $reporte->margen_superior, $reporte->margen_derecho, $reporte->margen_inferior));
                $html2pdf->writeHTML(utf8_decode($texto));
                if(!$return)$html2pdf->Output($reporte->nombre.'.pdf');
                else return $html2pdf->Output('',TRUE);
                //echo $texto;
                }
                
            }
            else $this->loadView('404');
        }
        
        function agenda($reunion,$f,$texto,$invitado){
            $str = '<table style="width:100%; table-layout:fixed" cellspacing="0">';
            $str.= '
                <tr>
                    <td style="width:60%; border:1px solid #4f81bd; border-bottom:3px solid #4f81bd; padding:10px 5px;">Temas / Tareas</td>
                    <td style="width:20%; border:1px solid #4f81bd; border-bottom:3px solid #4f81bd; padding:10px 5px;">Responsable </td>
                    <td style="width:20%; border:1px solid #4f81bd; border-bottom:3px solid #4f81bd; padding:10px 5px;">Adjunto </td>
                </tr>';
            $this->db->join('user','user.id = temas.responsable');
            $this->db->select('concat(user.nombre," ",user.apellido) as responsable, temas.tema, temas.id, temas.adjunto',FALSE);
            foreach($this->db->get_where('temas',array('reunion'=>$reunion))->result() as $n=>$t){
                $str.= '
                <tr>
                    <td style="width:60%; border-top:1px solid black;"><b>'.romanic_number($n+1).'. '.$t->tema.'</b></td>
                    <td style="width:20%; border-top:1px solid black;"><b>'.$t->responsable.'</b></td>
                    <td style="width:20%; border-top:1px solid black;"><b>'.$t->adjunto.'</b></td>
                </tr>';
                
                $this->db->join('user','user.id = tareas.responsable','left');
                $this->db->select('concat(user.nombre," ",user.apellido) as responsable, tareas.tipo, tareas.entrega, tareas.nombre, tareas.id, tareas.adjunto',FALSE);
                $this->db->where('reunion = '.$reunion.' AND tema = '.$t->id.' AND (user = 0 OR user = '.$invitado.')',null,FALSE);
                foreach($this->db->get('tareas')->result() as $no=>$ta){
                    if($ta->tipo=='actividad'){
                        $str.= '
                        <tr>
                            <td valign="top" style="width:60%;"><b>'.($no+1).'. <img style="width:15px" src="'.base_url('img/'.$ta->tipo.'.png').'"> '.$ta->nombre.' Fecha Ent. '.date("d/m/Y",strtotime($ta->entrega)).'</b></td>
                            <td style="width:20%;"><b>'.$ta->responsable.'</b></td>
                            <td style="width:20%;"><b>'.$ta->adjunto.'</b></td>
                        </tr>';
                    }
                    else{
                        $str.= '
                        <tr>
                            <td style="padding:10px 5px;" colspan="3">'.($no+1).'. <img style="width:15px" src="'.base_url('img/'.$ta->tipo.'.png').'"> '.$ta->nombre.'</td>                            
                        </tr>';
                    }
                }
            }
            $str.= '</table>';
            return str_replace('{function="'.$f.'"}',$str,$texto);
        }
        
        function asistentes($reunion,$f,$texto){
            $str = '<table style="width:100%;" cellspacing="0">';
            $str.= '
                <tr>
                    <td style="width:25%; border:1px solid #4f81bd; padding:10px 5px;">Nombre</td>
                    <td style="width:25%; border:1px solid #4f81bd; padding:10px 5px;">Mail</td>
                    <td style="width:20%; border:1px solid #4f81bd; padding:10px 5px;">Puesto</td>
                    <td style="width:10%; border:1px solid #4f81bd; padding:10px 5px; text-align:center">Lider</td>
                    <td style="width:10%; border:1px solid #4f81bd; padding:10px 5px; text-align:center">Convoco</td>
                    <td style="width:10%; border:1px solid #4f81bd; padding:10px 5px; text-align:center">Asistio</td>
                </tr>';
            $this->db->select('concat(user.nombre," ",user.apellido) as user, user.email, user.puesto, invitados.lider, invitados.convoco, invitados.asistio',FALSE);
            $this->db->join('user','user.id = invitados.invitado');
            foreach($this->db->get_where('invitados',array('reunion'=>$reunion))->result() as $n=>$i){
                $b = $n%2==0?'#d3dfee':'';
                $lider = $i->lider==1?'X':'';
                $convoco = $i->convoco==1?'X':'';
                $asistio = $i->asistio==1?'X':'';
                $str.= '<tr style="background:'.$b.';">
                            <td style="width:25%; border:1px solid #4f81bd; padding:10px 5px;">'.$i->user.'</td>
                            <td style="width:25%; border:1px solid #4f81bd; padding:10px 5px;">'.$i->email.'</td>
                            <td style="width:20%; border:1px solid #4f81bd; padding:10px 5px;">'.$i->puesto.'</td>
                            <td style="width:10%; border:1px solid #4f81bd; padding:10px 5px; text-align:center">'.$lider.'</td>
                            <td style="width:10%; border:1px solid #4f81bd; padding:10px 5px; text-align:center">'.$convoco.'</td>
                            <td style="width:10%; border:1px solid #4f81bd; padding:10px 5px; text-align:center">'.$asistio.'</td>
                        </tr>';
            }
            $str.= '</table>';
            return str_replace('{function="'.$f.'"}',$str,$texto);
        }
        /*Cruds*/              
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */