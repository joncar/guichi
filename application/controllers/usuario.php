<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('panel.php');
class Usuario extends Panel {
        
	public function __construct()
	{
            parent::__construct();
            $this->load->library('pjmail');
	}
       
        public function index($url = 'main',$page = 0)
	{		
            parent::index();
	} 
        
        function usuarios()
        {
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap2');
            $crud->set_table('user');
            $crud->set_subject('Usuarios');
            //Fields            
            //unsets            
            //Displays            
            //Fields types            
            //Validations              
            //Callbacks
            $crud->set_field_upload('foto','files');
            $crud->required_fields('nombre','sexo','apellido','movil','email','empresa','puesto');
            $crud->set_rules('email','Email','required|valid_email');
            $crud->callback_after_insert(function($post,$id){
                get_instance()->db->insert('contactos',array('contacto'=>$id,'user'=>$_SESSION['user']));
                get_instance()->db->insert('empresas',array('user'=>$id,'nombre'=>'Personal'));
                get_instance()->db->update('user',array('status'=>1,'password'=>md5('12345678')),array('id'=>$id));
            });
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'registro';
            //$this->loadView($output);
        }
        
        function empresas()
        {
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap2');
            $crud->set_table('empresas');
            $crud->set_subject('Libreta');               
            //Fields            
            //unsets            
            //Displays            
            //Fields types            
            //Validations              
            //Callbacks            
            $crud->required_fields('nombre');
            $crud->set_lang_string('insert_success_message','Datos guardado con exito <script>setTimeout(function(){document.location.href="'.base_url('perfil').'"},2000)</script>');
            $crud->set_lang_string('update_success_message','Datos actualizados con exito <script>setTimeout(function(){document.location.href="'.base_url('perfil').'"},2000)</script>');
            $crud->unset_back_to_list();
            $crud->field_type('user','invisible');
            $crud->callback_before_insert(function($post){$post['user'] = $_SESSION['user']; return $post;});
            
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $this->loadView($output);
        }    
        
        function contactos($x = '',$y='')
        {                     
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap2');
            $crud->set_table('user');
            $crud->set_subject('Contacto');
            $crud->set_model('contactos');            
            //Fields            
            //unsets            
            //Displays            
            //Fields types            
            //Validations              
            //Callbacks
            $crud->field_type('password','invisible')
                 ->field_type('admin','invisible')
                 ->field_type('status','invisible');
            $crud->field_type('ciudad','invisible');
            $crud->field_type('direccion','invisible');
            $crud->field_type('sexo','dropdown',array('Hombre','Mujer'));
            $crud->set_field_upload('foto','files');
            $crud->required_fields('nombre','sexo','apellido','email','empresa');
            $crud->fields('email','nombre','apellido','sexo','movil');
            $crud->add_action('<i title="Eliminar contacto" style="color:red" class="fa fa-minus-circle"></i>','',base_url('eliminar_contacto/').'/','contacto');
            $crud->unset_columns('password','admin','status')
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_export()
                 ->unset_print();
            $crud->display_as('emp','Empresa');            
            $crud->callback_after_insert(function($post,$id){
                get_instance()->db->insert('contactos',array('contacto'=>$id,'user'=>$_SESSION['user']));
                get_instance()->db->insert('empresas',array('user'=>$id,'nombre'=>'Personal'));
                get_instance()->db->update('user',array('status'=>1,'password'=>md5('12345678')),array('id'=>$id));
            });
            $crud->columns('name','apellido','sexo','email','ciudad','movil','emp','puesto');
            if(!empty($x) && $x=='popup'){
                $crud->set_lang_string('insert_success_message','<script>window.opener.refresh_new_users(); window.close();</script>');
                $crud->set_rules('email','Email','required|valid_email|callback_exist_popup|is_unique[user.email]');
            }
            else
                $crud->set_rules('email','Email','required|valid_email|callback_exist|is_unique[user.email]');
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'contacto';            
            $this->loadView($output);
        }  
        
        function eliminar_contacto($id){
            if($this->db->get_where('contactos',array('user'=>$_SESSION['user'],'contacto'=>$id))->num_rows>0){
                $this->db->delete('contactos',array('user'=>$_SESSION['user'],'contacto'=>$id));
                header("Location:".base_url('contactos/success'));
            }
        }
        
        function invitados($x = '')
        {                     
            if(!empty($x) && is_numeric($x) && $x>0){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap2');
            $crud->set_table('invitados');
            $crud->set_subject('Invitado');            
            $crud->where('reunion',$x);
            $crud->set_relation('invitado','user','{nombre} {apellido}');
            //Fields            
            //unsets            
            //Displays            
            //Fields types            
            //Validations              
            //Callbacks    
            $crud->field_type('convoco','true_false',array('0'=>'No','1'=>'Si'));
            $crud->field_type('lider','true_false',array('0'=>'No','1'=>'Si'));
            $crud->field_type('asiste','dropdown',array('-1'=>'<span class="alert alert-danger">No asistira</span>','0'=>'<span class="alert alert-info">Sin confirmar</span>','1'=>'<span class="alert alert-success">Si</span>'));
            $crud->unset_edit()
                 ->unset_columns('reunion')
                 ->unset_delete()
                 ->unset_read()
                 ->unset_export()
                 ->unset_add()
                 ->unset_print();                        
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'contacto';
            $this->loadView($output);
            }
            else header("Location:".base_url('reuniones'));
        }  
        
        function abort($id){
            if($this->db->get_where('reuniones',array('user'=>$_SESSION['user'],'id'=>$id))->num_rows>0){
                $this->db->update('reuniones',array('status'=>-1),array('id'=>$id));
                $this->mails->send_cancelacion($id);
                header("Location:".base_url('reuniones/success'));
            }
        }
        
        function asist($id){
            if($this->db->get_where('invitados',array('invitado'=>$_SESSION['user'],'reunion'=>$id))->num_rows>0){
                $this->db->update('invitados',array('asiste'=>1),array('invitado'=>$_SESSION['user'],'reunion'=>$id));
                header("Location:".base_url('reuniones/success'));
            }
        }
        
        function nasist($id){
            if($this->db->get_where('invitados',array('invitado'=>$_SESSION['user'],'reunion'=>$id))->num_rows>0){
                $this->db->update('invitados',array('asiste'=>-1),array('invitado'=>$_SESSION['user'],'reunion'=>$id));
                header("Location:".base_url('reuniones/success'));
            }
        }
        
        function reuniones($x = '')
        {                     
            $crud = parent::reuniones();
            $crud->where('reuniones.user',$_SESSION['user']);
            $crud->add_action('<i class="fa fa-user" title="Ver invitados"></i> <br/>Lista de invitados','',base_url('invitados').'/','invitados');            
            $crud->add_action('<i class="fa fa-edit" title="Editar Reunión"></i> <br/>Editar Reunión','',base_url('reprogramar').'/','reprogramar');            
            $crud->add_action('<i class="fa fa-minus-circle" title="Cancelar reunion"></i> <br/>Cancelar reunión','',base_url('abort').'/','abort');                        
            $this->db->where('reportes.tipo','1');
            $crud = $this->get_reportes($crud);
            $output = $crud->render();            
            
            $crud = parent::reuniones();  
            $crud->set_model('invitaciones');
            $crud->add_action('<i class="fa fa-check" title="Confirmar asistencia"></i> <br/>Confirmar asistencia','',base_url('asist').'/','asist');            
            $crud->add_action('<i class="fa fa-minus-circle" title="No asistiras"></i> <br/>Confirmar Inasistencia','',base_url('nasist').'/','nasist');            
            $this->db->where('reportes.tipo','1');
            $crud = $this->get_reportes($crud);
            $output2 = $crud->render();
            
            $o = array(
            "view" => 'panel',
            "crud" => 'reunion',
            "css_files"=>$output->css_files,
            "js_files"=>$output->js_files,
            "output"=>$output->output,
            "output2"=>$output2->output
            );
            
            $this->loadView($o);
        }
        
        function upload_foto(){
            if(!empty($_POST['nombre']) && !empty($_SESSION['user'])){                      
                $user = $this->db->get_where('user',array('id'=>$_SESSION['user']));
                if(!empty($user->row()->foto) && file_exists('files/'.$user->row()->foto))
                    unlink('files/'.$user->row()->foto);
                $filepath = basename($_FILES['foto_file']['name']);
                        list($name,$extension) = explode('.',$filepath);
                        $name = substr(rand(0,1024),0,8).'-'.$name;
                        $filepath = $name.'.'.strtolower($extension);
                move_uploaded_file($_FILES['foto_file']['tmp_name'],'files/'.$filepath);                
                $this->db->update('user',array('foto'=>$filepath),array('id'=>$_SESSION['user']));    
                echo base_url('files/'.$filepath);
                $_SESSION['foto'] = $filepath;
                }
        }
        
        function perfil()
        {
            $data = array(
                'view'=>'perfil',
                'user'=> $this->db->get_where('user',array('id'=>$_SESSION['user']))->row(),
                'empresas'=>$this->db->get_where('empresas',array('user'=>$_SESSION['user']))
            );
            $this->loadView($data);
        }
        
        function reunionadd($ref = '')
        {     
            $free = $this->db->get('ajustes')->row()->reuniones_free;
            $hechas = $this->db->get_where('reuniones',array('user'=>$_SESSION['user'],'Month(created)'=>date("m"),'Year(created)'=>date("Y")));            
            if($free>$hechas->num_rows || $_SESSION['premium']==1 || $_SESSION['cuenta']==1){
                $data = array(
                    'view'=>'reunion',    
                    'user'=>$this->db->get_where('user',array('id'=>$_SESSION['user']))->row(),
                    'empresas'=>$this->db->get_where('empresas',array('user'=>$_SESSION['user'])),                                
                );
                $this->db->select('user.id, user.*');
                $this->db->join('contactos','user.id = contactos.contacto','left');
                $this->db->where('contactos.user',$_SESSION['user']);
                $this->db->or_where('user.id',$_SESSION['user']);
                $data['contactos'] = $this->db->get('user');
                $data['contactos'] = $this->querys->setIniciales($data['contactos']);
                $this->loadView($data);
            }
            else{
                $this->loadView(array('view'=>'empty','msj'=>'<div style="padding:30px; text-align:center" class="alert alert-warning">Se han agotado las reuniones que puede generar para este mes, puede comprar una membresia premium anual para crear reuniones ilimitadas <br/> <a href="'.base_url('upgrade').'" class="btn btn-success">Ir a la sección de pagos</a></div>'));
            }
        }
        
        function reprogramar($id,$new = '')
        {
            if(!empty($id) && is_numeric($id) && $id>0 && $this->db->get_where('reuniones',array('id'=>$id,'user'=>$_SESSION['user']))->num_rows>0){            
            $data = array(
                'view'=>'reunion',    
                'user'=>$this->db->get_where('user',array('id'=>$_SESSION['user']))->row(),
                'empresas'=>$this->db->get_where('empresas',array('user'=>$_SESSION['user'])),
                'reunion'=>$this->db->get_where('reuniones',array('id'=>$id))->row(),
                'invitados'=>$this->db->get_where('invitados',array('reunion'=>$id)),
                'temas'=>$this->db->get_where('temas',array('reunion'=>$id)),                
            );
            $this->db->select('user.id, user.*');
            $this->db->join('contactos','user.id = contactos.contacto','left');
            $this->db->where('contactos.user',$_SESSION['user']);
            $this->db->or_where('user.id',$_SESSION['user']);
            $data['contactos'] = $this->db->get('user');
            $data['contactos'] = $this->querys->setIniciales($data['contactos']);
            if(!empty($new))$data['new'] = 1;
            $this->loadView($data);
            }
            else header("Location:".base_url('reuniones'));
        }
        
        function reunion_validation(){
            foreach($_POST as $n=>$v){
                $this->form_validation->set_rules($n,$n,'required');
            }
            $str = '';
            if(empty($_POST['tema'][0]))$str.='<p>Debe agregar al menos un tema</p>';
            if(empty($_POST['invitado_contacto'][1]))$str.='<p>Debe agregar al menos 2 invitados</p>';
            if($this->form_validation->run() && empty($str)){
                echo '<textarea>'.json_encode(array('success'=>TRUE)).'</textarea>';
            }
            else echo '<textarea>'.json_encode (array('success'=>FALSE,'error_message'=>$this->form_validation->error_string().$str)).'</textarea>';
        }
        
        function reunion_add(){
            $data = array();
            $arrays = array('tema','responsable','invitado_contacto','inicial','convoco','lider','adjuntar','puesto');
            foreach($_POST as $n=>$v){
                if(!in_array($n,$arrays))$data[$n] = $v;
            }            
            $data['fecha'] = date("Y-m-d",strtotime(str_replace('/','-',$data['fecha'])));
            $data['user'] = $_SESSION['user'];
            //$data['nota'] = strtotime($data['fecha'].' '.$data['hora']).'>='. strtotime(date("Y-m-d H:i:s"));
            if($data['fecha']!=date("Y-m-d"))
            $data['recordatorio'] = date("Y-m-d H:i:s",strtotime('-1 days '.$data['fecha'].' '.$data['hora']));
            
            $free = $this->db->get('ajustes')->row()->reuniones_free;
            $hechas = $this->db->get_where('reuniones',array('user'=>$_SESSION['user'],'Month(created)'=>date("m"),'Year(created)'=>date("Y")));
            if($free>$hechas->num_rows || $_SESSION['premium']==1 || $_SESSION['cuenta']==1){
                $this->db->insert('reuniones',$data);
                $id = $this->db->insert_id();
                foreach($_POST['tema'] as $n=>$t){

                    $filepath = '';
                    if(!empty($_FILES['adjuntar']['name'][$n])){
                        $filepath = basename($_FILES['adjuntar']['name'][$n]);
                            list($name,$extension) = explode('.',$filepath);
                            $name = substr(rand(0,1024),0,8).'-'.$name;
                            $filepath = $name.'.'.$extension;
                        move_uploaded_file($_FILES['adjuntar']['tmp_name'][$n],'files/'.$filepath);
                    }
                    
                    $data = array(
                        'reunion'=>$id,
                        'tema'=>$_POST['tema'][$n],
                        'responsable'=>$_POST['responsable'][$n][0],
                        'adjunto'=>$filepath
                    );
                    $this->db->insert('temas',$data);
                    $tema_id = $this->db->insert_id();
                    foreach($_POST['responsable'][$n] as $r){
                        $this->db->insert('temas_responsables',array('id_tema'=>$tema_id,'id_user'=>$r));
                    }
                }

                foreach($_POST['invitado_contacto'] as $n=>$t){
                    $convoco = !empty($_POST['convoco'][$n])?1:0;
                    $lider = !empty($_POST['lider'][$n])?1:0;
                    $data = array(
                        'reunion'=>$id,
                        'invitado'=>$_POST['invitado_contacto'][$n],
                        'iniciales'=>$_POST['inicial'][$n],
                        'convoco'=>$convoco,
                        'lider'=>$lider,
                    );
                    $this->db->insert('invitados',$data);
                }
                $this->mails->send_invitaciones($id);
                
                echo '<textarea>'.json_encode (array('success'=>TRUE,'error_message'=>'')).'</textarea>';
            }
            else echo '<textarea>'.json_encode (array('success'=>FALSE,'error_message'=>'Reuniones agotadas para este mes, Compre la membresia premium')).'</textarea>';
        }
        
        function reunion_edit($id){
            if(!empty($id) && is_numeric($id) && $id>0 && $this->db->get_where('reuniones',array('id'=>$id,'user'=>$_SESSION['user']))->num_rows>0){
                $data = array();
                $arrays = array('tema','responsable','adjunto','invitado_contacto','inicial','convoco','lider','adjuntar','puesto');
                foreach($_POST as $n=>$v){
                    if(!in_array($n,$arrays))$data[$n] = $v;
                }            
                $data['fecha'] = date("Y-m-d",strtotime(str_replace('/','-',$data['fecha'])));
                $data['status'] = 0;
                $this->db->update('reuniones',$data,array('id'=>$id));
                
                $this->db->delete('temas',array('reunion'=>$id));
                $this->db->delete('invitados',array('reunion'=>$id));
                
                foreach($_POST['tema'] as $n=>$t){

                    $filepath = !empty($_POST['adjunto'][$n])?$_POST['adjunto'][$n]:'';
                    if(!empty($_FILES['adjuntar']['name'][$n])){
                        if(!empty($filepath))
                            unlink('files/'.$filepath);
                        $filepath = basename($_FILES['adjuntar']['name'][$n]);
                        list($name,$extension) = explode('.',$filepath);
                        $name = substr(rand(0,1024),0,8).'-'.$name;
                        $filepath = $name.'.'.$extension;
                        move_uploaded_file($_FILES['adjuntar']['tmp_name'][$n],'files/'.$filepath);
                    }
                    $data = array(
                        'reunion'=>$id,
                        'tema'=>$_POST['tema'][$n],
                        'responsable'=>$_POST['responsable'][$n][0],
                        'adjunto'=>$filepath
                    );
                    if(!empty($t)){
                        $this->db->insert('temas',$data);
                        $tema_id = $this->db->insert_id();
                        foreach($_POST['responsable'][$n] as $r){
                            $this->db->insert('temas_responsables',array('id_tema'=>$tema_id,'id_user'=>$r));
                        }
                    }                
                }

                foreach($_POST['invitado_contacto'] as $n=>$t){
                    $convoco = !empty($_POST['convoco'][$n])?1:0;
                    $lider = !empty($_POST['lider'][$n])?1:0;
                    $data = array(
                        'reunion'=>$id,
                        'invitado'=>$_POST['invitado_contacto'][$n],
                        'iniciales'=>$_POST['inicial'][$n],
                        'convoco'=>$convoco,
                        'lider'=>$lider,
                    );
                    if(!empty($t)){
                        $this->db->insert('invitados',$data);                        
                    }
                    $this->mails->send_invitaciones($id);
                }
            }
            else echo json_encode(array('success'=>'false'));
        }
        
        function get_email(){
            $this->form_validation->set_rules('email','Email','required|valid_email');
            if($this->form_validation->run()){
                $user = $this->db->get_where('user',array('email'=>$_POST['email']));
                if($user->num_rows>0){
                    $user = $user->row();
                    $user->status = TRUE;
                    echo json_encode($user);
                }
                else echo json_encode(array('status'=>FALSE));
            }
            else echo json_encode(array('status'=>FALSE));
        }
        
        function exist($email)
        {
            if(!empty($email) && $x!='ajax_list' && $x!='ajax_list_info' && $this->db->get_where('user',array('email'=>$email))->num_rows>0){
                $this->db->insert('contactos',array('contacto'=>$this->db->get_where('user',array('email'=>$email))->row()->id,'user'=>$_SESSION['user']));
                $this->form_validation->set_message('exist','<script>document.location.href="'.base_url('contactos/success').'"</script>');
                return false;
            }  
            else return true;
        }
        
        function exist_popup($email)
        {
            if(!empty($email) && $x!='ajax_list' && $x!='ajax_list_info' && $this->db->get_where('user',array('email'=>$email))->num_rows>0){
                $this->db->insert('contactos',array('contacto'=>$this->db->get_where('user',array('email'=>$email))->row()->id,'user'=>$_SESSION['user']));
                $this->form_validation->set_message('exist_popup','<script>window.opener.refresh_new_users(); window.close();</script>');
                return false;
            }  
            else return true;
        }
        
        function reunion($id,$return = FALSE){
            $success = '';
            $this->db->delete('temas',array('responsable'=>0));
            if(!empty($return) && is_string($return))
            {
                $success = 'Reunión cerrada con éxito';
                $return = FALSE;
            }
            
            $reunion = $this->db->get_where('reuniones',array('id'=>$id));
            if(!empty($id) && is_numeric($id) && $id>0 && $reunion->num_rows>0){                                
                $reunion = $reunion->row();
                $data = array(
                    'view'=>'reunionstart',
                    'reunion'=>$reunion,   
                    'empresas'=>$this->db->get_where('empresas',array('id'=>$reunion->libreta)), 
                    'tareas'=>$this->db->get_where('tareas',array('reunion'=>$id)),
                    'msj'=>$success
                );                                                
                $this->db->select('invitados.*, user.*, user.id as iduser');
                $this->db->join('user','user.id = invitados.invitado');
                $data['invitados'] = $this->db->get_where('invitados',array('reunion'=>$id));
                                
                $this->db->join('user','user.id = temas.responsable');
                $data['temas'] = $this->db->get_where('temas',array('reunion'=>$id));
                
                if(!$return)$this->loadView($data);
                else return $data;
            }
            else header("Location:".base_url('panel'));
        }
        
        function reunion_update($id_reunion,$accion,$data = '',$val = ''){
            switch($accion){
                case 'asistio':
                    $this->db->update('invitados',array('asistio'=>$val),array('invitado'=>$data,'reunion'=>$id_reunion));
                break;
                case 'validar_actividad':                    
                    $datos = array('tema','responsable','actividad','fecha','porcentaje');
                    foreach($datos as $d){
                        foreach($_POST[$d] as $n=>$p){
                            $this->form_validation->set_rules($d,$d,'required');                            
                        }
                    }                                        
                    if($this->form_validation->run())
                        echo '<textarea>'.json_encode(array('success'=>'true')).'</textarea>';
                    else
                        echo '<textarea>'.json_encode(array('success'=>'false','error_submit'=>$this->form_validation->error_string())).'</textarea>';
                break;
                case 'init':
                    if($this->db->get_where('invitados',array('asistio'=>1,'reunion'=>$id_reunion))->num_rows>0){
                    if($this->db->get_where('reuniones',array('id'=>$id_reunion,'status'=>0))->num_rows>0)
                    $this->db->update('reuniones',array('status'=>1),array('id'=>$id_reunion));                    
                    }
                    else{
                        $_SESSION['msj'] = 'Debe pasar lista antes de iniciar la reunión';
                    }
                    header("Location:".base_url('reunion/'.$id_reunion));
                break;
                case 'addactividad':
                    print_r($_POST);
                break;
                case 'addrow':
                    $user = $_POST['tipo']=='nota'?$_SESSION['user']:0;
                    $this->db->insert('tareas',array('tipo'=>$_POST['tipo'],'reunion'=>$id_reunion,'tema'=>$_POST['tema'],'priority'=>'99','user'=>$user));
                    echo $this->db->insert_id();                                                                
                break;
                case 'removerow':
                    $adjunto = $this->db->get_where('tareas',array('id'=>$_POST['id']))->row()->adjunto;
                    if(!empty($adjunto) && file_exists('files/'.$adjunto));
                    unlink('files/'.$adjunto);
                    $this->db->delete('tareas',array('id'=>$_POST['id']));                    
                break;
                case 'update':
                    $data = array();
                    foreach($_POST as $p=>$y){
                        if($p=='entrega')
                        {
                            $y = date("Y-m-d",strtotime(str_replace('/','-',$y)));                            
                            $siguiente = date("Y-m-d",strtotime("-15 days ".$y));
                            if((strtotime(date("Y-m-d"))-strtotime($y))>0)
                                $siguiente = date("Y-m-d",strtotime("+2 days ".$y));
                            $this->db->update('tareas',array('recordatorio'=>$siguiente),array('id'=>$_POST['id']));
                        }
                        $data[$p] = $y;
                    }
                    if(!empty($_FILES['adjunto']['name'])){
                        $adjunto = $this->db->get_where('tareas',array('id'=>$_POST['id'],'reunion'=>$id_reunion))->row()->adjunto;
                        if(!empty($adjunto) && file_exists('files/'.$adjunto))
                        unlink('files/'.$adjunto);
                        $filepath = basename($_FILES['adjunto']['name']);
                        list($name,$extension) = explode('.',$filepath);
                        $name = substr(rand(0,1024),0,8).'-'.$name;
                        $filepath = $name.'.'.$extension;
                        //Save file
                        move_uploaded_file($_FILES['adjunto']['tmp_name'],'files/'.$filepath);
                        $data['adjunto'] = $filepath;
                    }
                    $this->db->update('tareas',$data,array('id'=>$_POST['id'],'reunion'=>$id_reunion));                    
                    if(!empty($filepath))echo $filepath;
                break;
                case 'refresh': 
                    $datos = $this->reunion($id_reunion,TRUE);
                    $data = array('Selecciona un responsable');
                    foreach($datos['invitados']->result() as $i){
                        $data[$i->iduser] = $this->querys->get_iniciales($i);
                    }                    
                    $datos['data'] = $data;
                    echo $this->load->view('includes/_reunion',$datos);
                break;
                case 'ordenar':
                    $data = json_decode($_POST['data']);
                    foreach($data as $y){
                        $this->db->update('tareas',array('priority'=>$y->pos),array('id'=>$y->id));
                    }
                break;
                case 'close':                    
                    $this->db->update('reuniones',array('status'=>2),array('id'=>$id_reunion));
                    //Enviar pdf
                    $this->db->select('user.email, user.id');
                    $this->db->join('user','user.id = invitados.invitado');
                    $invitados = $this->db->get_where('invitados',array('reunion'=>$id_reunion))->result();
                    foreach($invitados as $u){
                        $mail = new PJmail(); 
                        $mail->setAllFrom('info@guichi.com.mx', "Guichi.com.mx");
                        $mail->addrecipient($u->email);
                        $mail->addsubject("Se ha culminado una reunión en guichi.com.mx"); 
                        $mail->text = "Se ha culminado la reunión pautada para esta fecha, se adjunta el documento resultante que se genero";
                        $mail->addbinattachement("minuta.pdf",$this->imprimir_reporte(1,$id_reunion,TRUE,$u->id)); 
                        $res = $mail->sendmail(); 
                    }
                    header("Location:".base_url('reunion/'.$id_reunion.'/success_close'));
                break;
            }
        }
        /*Cruds*/    
        function tareas($x = '',$y = '')
        {                
            if($x=='edit' && $this->db->get_where('tareas',array('id'=>$y,'responsable'=>$_SESSION['user']))->num_rows==0)
            {
                $_SESSION['msj'] = 'No tiene permiso para editar esta tarea';
                header("Location:".base_url('tareas'));
            }      
            else{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap2');
            $crud->set_table('tareas');
            $crud->set_subject('responsable');            
//            $crud->where('tareas.responsable',$_SESSION['user']);
//            $crud->where('tareas.tipo','actividad');
//            $crud->set_relation('tema','temas','tema');
            $crud->set_model('tareas');
            $crud->set_field_upload('adjunto','files');
            $crud->set_field_upload('progreso_adjunto','files');
            $crud->display_as('reunion','#nro. reunion');
            $data = array();
            for($i=0;$i<=100;$i+=10)
            $data[$i] = $i.' %';
            $crud->field_type('progreso','dropdown',$data);
            //Fields            
            //unsets            
            //Displays            
            //Fields types            
            //Validations              
            //Callbacks                
            $crud->edit_fields('progreso','progreso_explicacion','progreso_adjunto');
            $crud->required_fields('progreso','progreso_explicacion');
            $crud->columns('reunion','tem','resp','tarea','entrega','progreso','adjunto','progreso_explicacion','progreso_adjunto')
                 ->unset_delete()
                 ->unset_read()
                 ->unset_export()
                 ->unset_add()
                 ->unset_print();                        
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'tarea';
            $this->loadView($output);            
            }
        }  
        
        function calendario(){
            $this->loadView('calendario');
        }
        
        function upgrade()
        {
            $this->loadView('pagar');
        }
        
        function ipnlistener(){
            $data = array(
                'email'=>$_POST['payer_email'],
                'fecha'=>date("Y-m-d H:i:s"),
                'monto'=>$_POST['payment_gross'],
                'motivo'=>$_POST['item_name'],
                'user'=>$_POST['custom'],
                'status'=>$_POST['payment_status'],                
                'transaccion_id'=>$_POST['txn_id'],
                'operacion'=>'+'
            );
            
            $this->db->insert('depositos',$data);
            $this->db->update('user',array('final_membresia'=>date("Y-m-d",strtotime("+30 days ".date("Y-m-d")))),array('id'=>$_POST['custom']));
            $str = '<h1>Se ha recibido un pago por membresia</h1>';
            $str.= '<p>'.$_POST['payer_email'].' Ha comprado la membresia premium por un valor de: '.$_POST['payment_gross'].'</p>';
            $str.= '<h2>Mas detalles</h2>';
            foreach($_POST as $p=>$v)
                $str.= '<div>'.$p.': '.$v.'</div>';
            
            correo('joncar.c@gmail.com','Pago recibido',$str);
            correo('renesam23@hotmail.com','Pago recibido',$str);
        }
        
        function get_last_contacts(){
            $this->db->select('user.id, user.*');
            $this->db->join('contactos','user.id = contactos.contacto','left');
            $this->db->where('contactos.user',$_SESSION['user']);
            $this->db->or_where('user.id',$_SESSION['user']);
            $str = '';
            $iniciales = array();
            $user = $this->querys->setIniciales($this->db->get('user'));
            foreach($user->result() as $u){
                $str.= '<option value="'.$u->id.'">'.$u->nombre.' '.$u->apellido.'</option>';
                array_push($iniciales,array('id'=>$u->id,'puesto'=>$u->puesto,'iniciales'=>$u->iniciales));
            }
            echo json_encode(array('iniciales'=>$iniciales,'options'=>$str));
        }
        
        function changepassword(){
            if(empty($_POST))
            $this->loadView(array('view'=>'recover','log'=>'Si'));
            else{
                $this->form_validation->set_rules('passant','Password anterior','required');
                $this->form_validation->set_rules('pass','Password anterior','required');
                $this->form_validation->set_rules('pass2','Password anterior','required|matches[pass]');
                if($this->form_validation->run()){
                    $query = $this->db->get_where('user',array('id'=>$_SESSION['user']))->row()->password;
                    if($query == md5($_POST['passant'])){
                        $this->db->update('user',array('password'=>md5($_POST['pass'])));
                        $this->loadView(array('view'=>'recover','log'=>'Si','msj'=>$this->success('Contraseña cambiada con exito')));
                    }
                    else
                        $this->loadView(array('view'=>'recover','log'=>'Si','msj'=>$this->error('Contraseña anterior no coincide')));
                }
                else
                    $this->loadView(array('view'=>'recover','log'=>'Si','msj'=>$this->error($this->form_validation->error_string())));
            }
        }
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */