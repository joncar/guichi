<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('panel.php');
class Admin extends Panel {
        
	public function __construct()
	{
            parent::__construct();
            if($_SESSION['cuenta']!=1)
            header("Location:".base_url('panel'));
	}
       
        public function index($url = 'main',$page = 0)
	{		
            $this->loadView('admin');
	}                                
                
        function perfil()
        {
            echo 'perfils';
        }
        
        function ajustes(){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap2');
            $crud->set_table('ajustes');
            $crud->set_subject('Ajustes');
            //Fields            
            //unsets            
            //Displays            
            //Fields types          
            $crud->field_type('moneda','enum',array('USD','MXN'));  
            $crud->unset_add()
                 ->unset_delete()
                 ->unset_export()
                 ->unset_print()
                 ->unset_read();
            //Validations              
            //Callbacks            
            $crud->required_fields('moneda','cuenta_paypal');
            $crud->set_rules('cuenta_paypal','Cuenta Paypal','required|valid_email');            
            $output = $crud->render();
            $output->view = 'admin';
            $output->crud = 'user';
            $this->loadView($output);
        }
        
        function banner(){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap2');
            $crud->set_table('banner');
            $crud->set_subject('Banner');
            //Fields            
            //unsets            
            //Displays            
            //Fields types                      
            $crud->unset_export()
                 ->unset_print()
                 ->unset_read();
            
            $crud->set_field_upload('imagen','img');
            //Validations              
            //Callbacks            
            $crud->required_fields('imagen');            
            $output = $crud->render();
            $output->view = 'admin';
            $output->crud = 'user';
            $this->loadView($output);
        }
        
        function usuarios($x = '',$y = ''){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap2');
            $crud->set_table('user');
            $crud->set_subject('Usuarios');
            //Fields            
            //unsets            
            //Displays            
            //Fields types  
            $crud->field_type('status','true_false',array('0'=>'Bloqueado','1'=>'Activo'));          
            $crud->field_type('admin','true_false',array('0'=>'Usuario','1'=>'Administrador'));
            $crud->unset_delete();
            //Validations              
            //Callbacks
            $crud->columns('foto','nombre','apellido','email','ciudad','movil','status','admin','final_membresia');
            $crud->set_field_upload('foto','files');
            $crud->required_fields('usuario','nombre','sexo','apellido','movil','email','empresa','puesto','admin','status','password');
            
            if(!empty($_POST) && (empty($y) || is_numeric($y) && $_POST['email']!=$this->db->get_where('user',array('id'=>$y))->row()->email))
            $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');            
            if(!empty($_POST) && (empty($y) || is_numeric($y) && $_POST['usuario']!=$this->db->get_where('user',array('id'=>$y))->row()->usuario))
            $crud->set_rules('usuario','Usuario','required|is_unique[user.usuario]');            
            
            $crud->set_rules('password','Password','required|min_length[8]');            
            $crud->callback_before_insert(function($row){
                $row['password'] = md5($row['password']);
                return $row;
            });
            $crud->callback_before_update(function($row,$primary){
                if($this->db->get_where('user',array('id'=>$primary))->row()->password!=$row['password'])
                $row['password'] = md5($row['password']);
                return $row;
            });
            $output = $crud->render();
            $output->view = 'admin';
            $output->crud = 'user';
            $this->loadView($output);
        }
        
         function reuniones($x = '',$y = ''){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap2');
            $crud->set_table('reuniones');
            $crud->set_subject('Reunion');
            $crud->set_relation('libreta','empresas','nombre');
            $crud->set_relation('user','user','{nombre} {apellido}');
            $crud->display_as('user','Creador');
            $crud->set_relation('libreta','empresas','nombre');
            $crud->columns('user','fecha','hora','duracion','libreta','lugar','titulo');
            $crud->unset_delete();
            //Fields            
            //unsets            
            //Displays            
            //Fields types              
            //Validations              
            //Callbacks            
            $output = $crud->render();
            $output->view = 'admin';
            $output->crud = 'user';
            $this->loadView($output);
        }
        
        function tareas($x = '',$y = ''){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap2');
            $crud->set_table('tareas');
            $crud->set_subject('Tareas');
            $crud->where('tipo','actividad');
            $crud->where('responsable !=','0');
            $crud->unset_delete();
            $crud->set_relation('responsable','user','{nombre} {apellido}');
            $crud->columns('responsable','nombre','entrega','progreso','adjunto','recordatorio');
            $crud->set_field_upload('adjunto','files');
            $crud->unset_add()
                 ->unset_edit()
                 ->unset_read()
                 ->unset_export()
                 ->unset_print();
            //Fields            
            //unsets            
            //Displays            
            //Fields types              
            //Validations              
            //Callbacks            
            $output = $crud->render();
            $output->view = 'admin';
            $output->crud = 'user';
            $this->loadView($output);
        }
        
        function depositos($x = '',$y = ''){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap2');
            $crud->set_table('depositos');
            $crud->set_subject('Depositos');
            $crud->set_relation('user','user','{nombre} {apellido}');
            //Fields            
            //unsets            
            //Displays            
            //Fields types              
            $crud->unset_delete();
            //Validations              
            //Callbacks            
            $output = $crud->render();
            $output->view = 'admin';
            $output->crud = 'user';
            $this->loadView($output);
        }
        
        function reportes($x = '', $y = '')
        {            
            $crud = new ajax_grocery_CRUD();            
            $crud->set_theme('bootstrap2');
            $crud->set_table('reportes');
            $crud->set_subject('Reportes');                            
            $crud->field_type('tipo','true_false',array('0'=>'General','1'=>'Especifico'));
            $crud->field_type('orientacion','dropdown',array('P'=>'Vertical','L'=>'Horizontal'));
            $crud->field_type('papel','dropdown',array('Letter'=>'Carta','A4'=>'A4','otro'=>'Medida'));
            $crud->field_type('variables','products',array(
                'table'=>'reportes_variables',
                'dropdown'=>'tipo_dato',
                'relation_field'=>'reporte',
                'source'=>array('input'=>'input','date'=>'date'),                
            ));
            $crud->unset_fields('id','funcion');
            $crud->add_action('<i title="Imprimir reporte" class="fa fa-print"></i>','',base_url('panel/imprimir_reporte').'/');
            $crud->callback_add_field('margen_izquierdo',function($val){return form_input('margen_izquierdo',5,'id="field-margen_izquierdo"');});
            $crud->callback_add_field('margen_derecho',function($val){return form_input('margen_derecho',5,'id="field-margen_derecho"');});
            $crud->callback_add_field('margen_superior',function($val){return form_input('margen_superior',5,'id="field-margen_superior"');});
            $crud->callback_add_field('margen_inferior',function($val){return form_input('margen_inferior',8,'id="field-margen_inferior"');});
            $crud->required_fields('controlador','nombre','contenido','tipo','orientacion','papel','margen_izquierdo','margen_derecho',
                  'margen_superior','margen_inferior');
            $crud->columns('tipo','nombre','controlador');
            if(!empty($_POST) && !empty($_POST['papel']) && $_POST['papel']=='otro')
            {
                $crud->set_rules('ancho','Ancho','required');
                $crud->set_rules('alto','Alto','required');
            }
            
            $crud->callback_before_insert(function($post){$post['contenido'] = base64_encode($post['contenido']); return $post;});
            $crud->callback_before_update(function($post){$post['contenido'] = base64_encode($post['contenido']); return $post;});
            $this->db->where('reportes.tipo','1');
            $crud = $this->get_reportes($crud);
            $output = $crud->render();
            $output->view = 'admin';
            $output->crud = 'reportes';
            $output->title = 'Reportes';
            $this->loadView($output);
        }
        /*Cruds*/              
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */