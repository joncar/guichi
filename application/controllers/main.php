<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
session_start();
class Main extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */        
	public function __construct()
	{
		parent::__construct();                
		$this->load->helper('url');
                $this->load->helper('html');
                $this->load->helper('h');                
                $this->load->database();
                $this->load->model('user');                        
                $this->load->model('querys');
                $this->load->model('mails');
                $this->load->library('grocery_crud');                
                $this->load->library('ajax_grocery_crud');
                ini_set('display_errors',true);
                setlocale(LC_ALL,"es_ES");
                //ini_set('date.timezone', 'America/Mexico');
                date_default_timezone_set('America/Mexico_City');
	}
        
        public function index()
	{
            if(empty($_SESSION['user']) || $_SESSION['user']=='clean')
            {
                require_once('registro.php');
                $registro = new registro();
                $registro = $registro->index();
                //$this->loadView($registro);
            }
            else
            {                
                header("Location:".base_url('panel'));
            }
	}
        
        public function success($msj)
	{
		return '<div class="alert alert-success">'.$msj.'</div>';
	}

	public function error($msj)
	{
		return '<div class="alert alert-danger">'.$msj.'</div>';
	}
        
        public function login()
	{
		if(!$this->user->log)
		{	
			if(!empty($_POST['usuario']) && !empty($_POST['pass']))
			{
				$this->db->where('email',$this->input->post('usuario'));
				$r = $this->db->get('user');
				if($this->user->login($this->input->post('usuario',TRUE),$this->input->post('pass',TRUE)))
				{
					if($r->num_rows>0 && $r->row()->status==1)
					{
                                            if(!empty($_POST['remember']))$_SESSION['remember'] = 1;
                                            if(empty($_POST['redirect']))
                                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.site_url('panel').'"</script>');
                                            else
                                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.$_POST['redirect'].'"</script>');
					}
					else $_SESSION['msj'] = $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
				}
                                else $_SESSION['msj'] = $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
			}
			else
                            $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');
                        
                        if(!empty($_SESSION['msj']))
                            header("Location:".base_url());
		}
	}

	public function unlog()
	{
		$this->user->unlog();
                //$_SESSION['user'] = 'clean';
                header("Location:".site_url());
	}
        
        public function loadView($param = array('view'=>'main'))
        {
            if(is_string($param))
            $param = array('view'=>$param);
            $this->load->view('template',$param);
        }
		
	public function loadViewAjax($view,$data = null)
        {
            $view = $this->valid_rules($view);
            $this->load->view($view,$data);
        }
        
        function cron(){
            $this->mails->send_reuniones();
            $this->mails->send_tarea();
            //echo "asd";
            /*correo('joncar.c@gmail.com','Ejecuto el cron','Ejecuto el cron');*/
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */