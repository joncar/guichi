<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('rest.php');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
class Api extends Rest {
    function posting()
    {        
        if(!empty($this->_post_args)){
            foreach($this->_post_args as $x=>$y)
                $_POST[$x] = $this->_xss_clean ($y,true);
        }
    }
    function generos_get()
    {
        $data = array();
        foreach($this->db->get('generos')->result() as $g)
            array_push($data,array('id'=>$g->id,'nombre'=>$g->nombre));
        $this->response($data);
    }
    
    function discos_post()
    {
        $this->posting();
        $data = array();
        $this->form_validation->set_rules('id','ID','required');
        if($this->form_validation->run()){
        foreach($this->db->get_where('discos',array('genero'=>$_POST['id']))->result() as $g)
            array_push($data,array('id'=>$g->id,'nombre'=>$g->nombre,'foto'=>base_url('files/'.$g->foto),'autor'=>$g->autor,'fecha_album'=>$g->fecha_album));
        }
        $this->response($data);
    }
    
    function disco_post()
    {
        $this->posting();
        $data = array();
        $this->form_validation->set_rules('id','ID','required');
            if($this->form_validation->run()){                
            $this->db->join('discos','discos.id = canciones.disco','inner');
            $this->db->select('canciones.*, discos.nombre as disc, discos.foto as foto');
            foreach($this->db->get_where('canciones',array('discos.id'=>$_POST['id']))->result() as $g)
                array_push($data,array('disco'=>$g->disc,'id'=>$g->id,'nombre'=>$g->nombre,'foto'=>base_url('files/'.$g->foto),'url'=>base_url('audios/'.$g->url)));
            }
        $this->response($data);
    }
    
    function eventos_post(){
        
        $data = array();
        foreach($this->db->get('eventos')->result() as $g)
            array_push($data,array('id'=>$g->id,'titulo'=>$g->titulo,'foto'=>base_url('files/'.$g->foto),'mapa'=>$g->mapa,'fecha'=>$g->fecha,'direccion'=>$g->direccion));
        $this->response($data);
    }
    
     function eventosShow_post(){
        $this->posting();
        $data = array();
        $this->form_validation->set_rules('id','ID','required');
            if($this->form_validation->run()){                            
                $data = $this->db->get_where('eventos',array('id'=>$this->input->post('id')))->row();
                $data->foto = base_url('files/'.$data->foto);
            }
        $this->response($data);
    }
    
     function sendevent_post(){
        $this->posting();
        $data = array();
        $this->form_validation->set_rules('titulo','Titulo','required');
        $this->form_validation->set_rules('descripcion','Descripcion','required');
        $this->form_validation->set_rules('fecha','Fecha','required');
        $this->form_validation->set_rules('direccion','Direccion','required');
            if($this->form_validation->run()){                            
                $data = array(
                    'titulo'=>$this->input->post('titulo'),
                    'descripcion'=>$this->input->post('descripcion'),
                    'fecha'=>date("Y-m-d",strtotime(str_replace("/","-",$this->input->post('fecha')))),
                    'direccion'=>$this->input->post('direccion'),
                    'foto'=>$_FILES['foto']['name']
                );
                $this->db->insert('eventos',$data);
                $filepath = 'files/'.basename($_FILES['foto']['name']);

                //Save file
                move_uploaded_file($_FILES['foto']['tmp_name'], $filepath);
            }
        $this->response(array('status'=>'success'));
    }
}