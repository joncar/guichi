<?= $output ?>
<script>
    $("#field-email").blur(function(){
        if($(this).val()!=''){
            $(this).attr('readonly',true);
            $.post('<?= base_url('get_email')?>',{email:$(this).val()},function(data){
                data = JSON.parse(data);
                if(data.status){
                    $("#field-usuario").val(data.usuario);                    
                    $("#field-puesto").val(data.puesto);
                    $("#field-nombre").val(data.nombre);
                    $("#field-apellido").val(data.apellido);
                    $("#field-sexo").val(data.sexo);
                    $("#field-movil").val(data.movil);
                }
                
                $("#field-email").attr('readonly',false);
            });
        }
    });
    
    $(".contacto").click(function(e){
        if(!confirm('Seguro que desea eliminar este contacto?'))e.preventDefault();
    });
</script>