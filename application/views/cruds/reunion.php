<style type="text/css">
.ui-button-text-icon-primary .ui-button-text, .ui-button-text-icons .ui-button-text{
	padding: 4px !important;
}
</style>
<div class='row'>
<h2>Lista de Reuniones (Creadas)</h2>
<?= $output ?>
</div>
<div class='row'>
<h2>Lista de Reuniones (Invitaciones)</h2>
<?= $output2 ?>
</div>

<script>
    $(document).ready(function(){
        $(".abort").click(function(e){
            if(!confirm('Seguro que desea cancelar esta reunion?'))
                e.preventDefault();
        })
        
        $(".asist").click(function(e){
            if(!confirm('Seguro que desea asistir a esta reunion?'))
                e.preventDefault();
        })
        
        $(".nasist").click(function(e){
            if(!confirm('Seguro que no desea asistir a esta reunion?'))
                e.preventDefault();
        })
    });
</script>