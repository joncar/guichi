<section class="row" style="margin-right: 0px; margin-right:0px;">
    <div class="col-sm-4 col-sm-offset-3 col-xs-12">
        <div class="well">
            <b>Para recuperar su contraseña debe indicarnos los siguientes datos:</b>
            <form action="<?= base_url('registro/forget') ?>" method="post" onsubmit="return validar(this)" role="form" class="form-horizontal">
                <?= !empty($_SESSION['msj'])?$_SESSION['msj']:'' ?>
                <?= !empty($msj)?$msj:'' ?>
                <?= input('email','Email','email') ?>
                <div align='center'><button type="submit" class="btn btn-success">Recuperar contraseña</button></div>
            </form>
        </div>
    </div>    
</section>