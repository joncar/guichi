<?php if(!empty($_SESSION['msj'])):  ?>
    <div class="alert alert-danger"><?= $_SESSION['msj'] ?></div>
<?php $_SESSION['msj']=''; endif ?>
<?php $this->load->view('includes/reunion') ?>
<script>      
    var url = '<?= base_url() ?>';
    var aux = '';
    var dat = [];
    $(document).ready(function(){
        $(document).on('keydown','textarea',function(e){
            if(e.which==13)$(this).val($(this).val()+"\n");
        });
        
        sort();
    });
    <?php if($reunion->user==$_SESSION['user'] && $reunion->status==1): ?>       
    function enviaractividad(obj){       
        url_validation = '<?= base_url('reunion_update/'.$reunion->id.'/validar_actividad') ?>';
        url = '<?= base_url('reunion_update/'.$reunion->id.'/addactividad') ?>';
        $("button[type='submit']").html('Guardando');
        $("button[type='submit']").attr('disabled',true);
        val(obj);        
        return false;        
    }
    
    $(document).on('success_submit',function(e,data){
        $("button[type='submit']").html('Guardar Actividades');
        $("button[type='submit']").attr('disabled',false);
        $.post('<?= base_url('reunion_update/'.$reunion->id.'/refresh') ?>',{},function(data){
            $("#_reunion").html(data);
            evento();
        });
    })
    
    
    $(document).ready(function(){
        evento();                
    });
    
    function evento(){      
        var row = undefined;
        $(".add").click(function(e){
            e.preventDefault();
            <?php if($reunion->status==1): ?>
            row = $(this);
            $.post('<?= base_url('reunion_update/'.$reunion->id.'/addrow') ?>',{tema:$(this).data('rel'),tipo:$(this).data('val')},function(data){
                row.parent('div').parent('div').parent('div').find('.lista').append(addRowT(row.attr('data-val'),data));                
                $(".remove").click(removerowevent); 
                $(".invitadoss textarea,.invitadoss select,.invitadoss input").change(blurevent);
                initCalendario();
                $('input, textarea').keypress(function(event){
                    var enterOkClass =  $(this).attr('class');
                    if (event.which == 13 && enterOkClass != 'enterSubmit') {
                        event.preventDefault();
                        return false;   
                    }
                });
            });
            <?php else: ?>
            alert("Para editar esta reunión primero debe aperturarla");
            <?php endif ?>
        });   
        
        $('input, textarea').keypress(function(event){
            var enterOkClass =  $(this).attr('class');
            if (event.which == 13 && enterOkClass != 'enterSubmit') {
                event.preventDefault();
                return false;   
            }
        });
        
        $(".remove").click(removerowevent); 
        $(".invitadoss textarea,.invitadoss select,.invitadoss input").change(blurevent);                                 
        initCalendario();
        
    }
    
    function removerowevent(e){
        e.preventDefault();
        row = $(this);
        $.post('<?= base_url('reunion_update/'.$reunion->id.'/removerow') ?>',{id:$(this).data('rel')},function(data){
            row.parent('div').parent('div').parent('li').remove();
        });
    }
    
    function blurevent(e){
       switch($(this).attr('name')){
           case 'nombre':
               data = {id:$(this).data('rel'),nombre:to_html($(this).val())};
           break;
           case 'progreso':
               data = {id:$(this).data('rel'),progreso:to_html($(this).val())};
           break;
           case 'entrega':
               data = {id:$(this).data('rel'),entrega:to_html($(this).val())};
           break;
           case 'responsable':
               data = {id:$(this).data('rel'),responsable:to_html($(this).val())};
           break;
       }
       if($(this).attr('type')=='file'){
            var formData = new FormData();            
            $(this).parent('div').find("label").hide();
            $(this).parent('div').find("#loading").show();
            row = $(this).parent('div').find('a');
            formData.append('id',$(this).data('rel'));
            formData.append('adjunto',$(this).prop('files')[0]);
            $.ajax({
            url: '<?= base_url('reunion_update/'.$reunion->id.'/update') ?>',
            data: formData,
            context: document.body,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST'}).always(function(data){
            row.parent('div').find("label").show();
            row.parent('div').find("#loading").hide();
            row.attr('href',url+'files/'+data);
            row.html('<i class="fa fa-download"></i> '+data);
            });
       }
       else
       $.post('<?= base_url('reunion_update/'.$reunion->id.'/update') ?>',data,function(data){});
    }
    
    function initCalendario()
    {
        $(".datetime-input").removeClass('hasDatepicker').removeAttr('id');
        
        $(".datetime-input").datetimepicker({
            timeFormat: "",
                    dateFormat: "dd/mm/yy",
                    showButtonPanel: true,
                    changeMonth: true,
                    changeYear: true
        });

        $(".datetime-input-clear").button();

        $(".datetime-input-clear").click(function(){
                $(this).parent().find(".datetime-input").val("");
                return false;
        });	
    }
    
    function addRowT(type,id)
    {         
        switch(type){
            case 'normal':
            case 'nota':
                icon = 'glyphicon glyphicon-list-alt';
                if(type=='nota')icon = 'fa fa-file-o';
                color= 'green';
                width = '95%';
                col = '';
                col2 = '';
                col3 = '';           
                placeholder = type=='nota'?'Nota personal con respecto a este punto y solo ser�� visible por ti':'Texto citado que corresponde al desarrollo normal del tema'
            break;
            case 'important':
                icon = 'fa fa-exclamation-circle';
                color= 'red';
                col = '';
                col2 = '';
                col3 = '';
                width = '95%';
                placeholder = 'Texto citado que corresponde a una información importante en el desarrollo del tema'
            break;
            case 'important2':
                icon = 'fa fa-lightbulb-o';
                color= '#66ccff';
                col = '';
                col2 = '';
                col3 = '';
                width = '95%';
                placeholder = 'Texto citado que corresponde a una idea importante en el desarrollo del tema'
            break;
            case 'actividad':
                $("#responsableField select").attr('data-rel',id);
                icon = 'fa fa-cog'
                color = 'green';
                col = '<input data-rel="'+id+'" type="number" readonly name="progreso" value="0" class="form-control" style="width:60px; display:inline-block;">%';
                col2 = $("#responsableField").html();
                col+= ' Entrega: <input type="text"  data-rel="'+id+'" name="entrega" class="form-control datetime-input" style="display:inline-block; width:140px" placeholder="dd/mm/yyyy">'
                col3 = '<label for="adjuntar_'+id+'"><i class="fa fa-upload"></i></label><img id="loading" style="width:40px; display:none;" src="http://guichi.com.mx/img/loading.gif"><input type="file" id="adjuntar_'+id+'" name="adjuntar" data-rel="'+id+'" style="display:none"> | <a href="" target="_new"></a> ';
                placeholder = "Describe la actividad a desarrollar";
                width = '50%';
            break;
        }
        str = '<li style="font-weight:bold" data-rel="'+id+'">';
        str+= '<div style="margin-top: -18px; margin-left: 10px;" class="row"><div class="col-xs-7">';
        str+= '<i style="color:'+color+'; cursor:move; vertical-align: top;" class="'+icon+'"></i> <textarea data-rel = "'+id+'" name="nombre" style="height:40px; display:inline-block; width:'+width+'" class="form-control" placeholder="'+placeholder+'"></textarea> '+col;
        str+= '</div>';
        str+= '<div class="col-xs-2">'+col2+'</div>';
        str+= '<div class="col-xs-2">'+col3+'</div>';
        str+= '<div class="col-xs-1"><a style="color:red" href="#" data-rel="'+id+'" class="remove"><i class="glyphicon glyphicon-remove"></i></a></div>';
        str+= '</div></li>';
        return str;
    }
    <?php else: ?>    
    <?php if($reunion->status!=2 || $reunion->status!=1): ?>   
    $(".nota").click(notaclick); 
    sort();
    function notaclick(e){
        e.preventDefault();
        row = $(this);
        $.post('<?= base_url('reunion_update/'.$reunion->id.'/addrow') ?>',{tema:$(this).data('rel'),tipo:$(this).data('val')},function(data){
            row.parent('div').parent('div').parent('div').find('.lista').append(addRowT(row.attr('data-val'),data));                            
            $(".invitadoss textarea,.invitadoss select,.invitadoss input").change(blurevent);   
            $(".removenota").click(removevent);
        });
    }
    function removevent(e){
        e.preventDefault();
        row = $(this);
        $.post('<?= base_url('reunion_update/'.$reunion->id.'/removerow') ?>',{id:$(this).data('rel')},function(data){
            row.parent('div').parent('div').parent('li').remove();
        });
    }

    function addRowT(type,id)
    {         
        switch(type){                
            case 'nota':                    
                icon = 'fa fa-file-o';
                color= 'green';
                width = '95%';
                col = '';
                col2 = '';
                col3 = '';           
                placeholder = 'Nota personal con respecto a este punto y solo ser�� visible por ti';
            break;                
        }
        str = '<li style="font-weight:bold" data-rel="'+id+'">';
        str+= '<div style="margin-top: -18px; margin-left: 10px;" class="row"><div class="col-xs-7">';
        str+= '<i style="color:'+color+'; cursor:move; vertical-align: top;" class="'+icon+'"></i> <textarea  data-rel = "'+id+'" name="nombre" style="height:40px; display:inline-block; width:'+width+'" class="form-control notat" placeholder="'+placeholder+'"></textarea> '+col;
        str+= '</div>';
        str+= '<div class="col-xs-2">'+col2+'</div>';
        str+= '<div class="col-xs-2">'+col3+'</div>';
        str+= '<div class="col-xs-1"><a style="color:red" href="#" data-rel="'+id+'" class="removenota"><i class="glyphicon glyphicon-remove"></i></a></div>';
        str+= '</div></li>';
        $(".notat").change(blurevent);
        return str;
    }
    
    function blurevent(e){
        data = {id:$(this).data('rel'),nombre:$(this).val()}
        $.post('<?= base_url('reunion_update/'.$reunion->id.'/update') ?>',data,function(data){});
    }
    $(".removenota").click(removevent);
    $(".notat").change(blurevent);
    refrescar();
    function refrescar(){
        $.post('<?= base_url('reunion_update/'.$reunion->id.'/refresh') ?>',{},function(data){
            if(!$("textarea").is(":focus")) {
              $("#_reunion").html(data);
              $(".notat").change(blurevent);
              $(".nota").click(notaclick);
              $(".removenota").click(removevent);
              $(".add").click(function(e){e.preventDefault(); <?php if($_SESSION['user']==$reunion->user): ?> alert("Para editar esta reunión primero debe aperturarla"); <?php else: ?> alert("Funcion reservada solo para el lider"); <?php endif ?>});
              $("input, textarea, select").attr('readonly',true);
              $(".notat").attr('readonly',false);
              sort();
               <?php if($_SESSION['user']==$reunion->user): ?>
                    $(".asistio").on('click',function(){        
                        asistio = 0;
                        if($(this).prop('checked')){
                            asistio = 1;
                        }
                        $.post('<?= base_url('reunion_update/'.$reunion->id.'/asistio/') ?>/'+$(this).attr('data-val')+"/"+asistio,{},function(data){});
                    });  
                <?php endif ?>
            }        
            setTimeout(refrescar,5000);
        });
    }
    <?php endif ?>
    <?php endif ?>
    <?php if($reunion->status==2): ?>
        $(document).ready(function(){
            $("input, textarea, select").attr('readonly',true);
            $(".nota, .notat").attr('readonly',false);
        })
    <?php endif ?> 
    function to_html(str){        
        return str.replace(/\n/g, '<br/>');
    }
    
    //$(".lista").sortable();        
    function sort(){
        $(".lista").sortable({
            stop: function(event,ui){
                <?php if($reunion->status==2): ?>        
                  if(ui.item.find('textarea').hasClass('nota') || ui.item.find('textarea').hasClass('notat')){
                <?php endif ?>
                row = $(this).find('li').nextAll().length;
                aux = 0;
                dat = [];
                console.log(ui);
                $(this).find('li').each(function(){
                    aux++;
                    dat.push({id:$(this).data('rel'),pos:aux});
                    if(aux==row+1)
                    {                                             
                        $.post('<?= base_url('reunion_update/'.$reunion->id.'/ordenar') ?>',{data:JSON.stringify(dat)},function(data){

                        });
                    }
                })
                <?php if($reunion->status==2): ?>        
                  }
                  else return false;
                <?php endif ?>            
            }
        });
    }
</script>