<div class="row col-xs-4" align="center">
<form action="<?= empty($log)?base_url('registro/forget'):base_url('changepassword') ?>" method="post" onsubmit="return validar(this)" role="form" class="form-horizontal">
    <?= !empty($msj)?$msj:'' ?>
    <?php if(empty($log)): ?>
    <input type="email" name="email" id="email" data-val="required" class="form-control" data-val='required' value="<?= $_SESSION['email'] ?>" readonly><br/>
    <?php else: ?>
    <input type="password" class="form-control" name="passant" id="pass" placeholder="Password Anterior" data-val='required'><br/>
    <?php endif ?>
    <input type="password" class="form-control" name="pass" id="pass" placeholder="Nuevo Password" data-val='required'><br/>
    <input type="password" class="form-control" name="pass2" id="pass2" placeholder="Repetir Password" data-val='required'><br/>
    <?php if(empty($log)): ?><input type="hidden" name="key" value="<?= $key ?>"><?php endif ?>
    <button type="submit" class="btn btn-success">Recuperar contraseña</button>
</form>
    </div>