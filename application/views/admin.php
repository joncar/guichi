<div class="container">
    <div class="col-xs-12 col-sm-2">
        <ul class="nav nav-pills nav-stacked" role="tablist">
            <li><a href="<?= base_url('admin/ajustes') ?>"><i class="fa fa-wrench"></i> Ajustes</a></li>
            <li><a href="<?= base_url('admin/usuarios') ?>"><i class="fa fa-user"></i> Usuarios</a></li>
            <li><a href="<?= base_url('admin/reuniones') ?>"><i class="fa fa-group"></i> Reuniones</a></li>
            <li><a href="<?= base_url('admin/tareas') ?>"><i class="fa fa-file-o"></i> Tareas</a></li>
            <li><a href="<?= base_url('admin/depositos') ?>"><i class="fa fa-money"></i> Pagos recibidos</a></li>
            <li><a href="<?= base_url('admin/reportes') ?>"><i class="fa fa-money"></i> Reportes</a></li>
            <li><a href="<?= base_url('admin/banner') ?>"><i class="fa fa-picture-o"></i> Banners</a></li>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-10">
        <?php if(!empty($crud))$this->load->view('cruds/'.$crud) ?>
    </div>
</div>