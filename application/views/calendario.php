<div class='container'>
    <section class="main">
        <div class="custom-calendar-wrap">
                <div id="custom-inner" class="custom-inner">
                        <div class="custom-header clearfix">
                                <nav>
                                    <span id="custom-prev" class="custom-prev"></span>
                                    <span id="custom-next" class="custom-next"></span>
                                </nav>
                                <h2 style='padding:0px; margin-bottom:0px; background:transparent' id="custom-month" class="custom-month"> <br/></h2>
                                <h3 style='margin-top: 1px;' id="custom-year" class="custom-year"></h3>
                        </div>
                        <div id="calendar" class="fc-calendar-container"></div>
                </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?= base_url('js/jquery.calendario.js') ?>"></script>
<script type="text/javascript">	
var codropsEvents = [];
$(document).ready(function(){
    <?php 
        $this->db->select('reuniones.id, reuniones.titulo, reuniones.fecha, reuniones.lugar, reuniones.hora, concat(user.nombre," ",user.apellido) as user',FALSE);
        $this->db->join('invitados','invitados.reunion = reuniones.id');
        $this->db->join('user','user.id = reuniones.user');
        $this->db->where("(user = '".$_SESSION['user']."' OR invitados.invitado= '".$_SESSION['user']."') AND reuniones.status != '-1'",null,TRUE);
        $this->db->group_by('reuniones.id');
    ?>
    <?php $data = array() ?>    
    <?php foreach($this->db->get('reuniones')->result() as $r): ?>
            //'<?= date("m-d-Y",strtotime($r->fecha)) ?>':'<a href="<?= base_url('reunion/'.$r->id) ?>"><?= $r->titulo.'<br/> a las '.$r->hora,'<br/> en '.$r->lugar.'<br/> por '.$r->user ?></a>',
    <?php $str = '<a href="'.base_url('reunion/'.$r->id).'">'.$r->titulo.' - '.$r->hora.' - Convocado por:'.$r->user.' - en: '.$r->lugar.'</a>' ?>
    <?php empty($data[date("m-d-Y",strtotime($r->fecha))])?$data[date("m-d-Y",strtotime($r->fecha))] = $str:$data[date("m-d-Y",strtotime($r->fecha))].='<br/>'.$str; ?>
    <?php endforeach ?>  
        codropsEvents = <?php echo json_encode($data) ?>    
        //codropsEvents = {"2014-11-15":"Reuni\u00f3n de Ventas<br\/>tirulo","2014-11-16":"Reuni\u00f3n de Prueba<br\/>Test<br\/>Test<br\/>T\u00edtulo de la reuni\u00f3n<br\/>Test<br\/>Testing evento<br\/>Testing evento<br\/>Testing evento","2014-11-17":"T\u00edtulo"}    
    init();
});
    
function init() {
    console.log(codropsEvents);
        var transEndEventNames = {
                        'WebkitTransition' : 'webkitTransitionEnd',
                        'MozTransition' : 'transitionend',
                        'OTransition' : 'oTransitionEnd',
                        'msTransition' : 'MSTransitionEnd',
                        'transition' : 'transitionend'
                },
                transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
                $wrapper = $( '#custom-inner' ),
                $calendar = $( '#calendar' ),
                cal = $calendar.calendario( {
                        onDayClick : function( $el, $contentEl, dateProperties ) {

                                if( $contentEl.length > 0 ) {
                                        showEvents( $contentEl, dateProperties );
                                }

                        },
                        caldata : codropsEvents,
                        displayWeekAbbr : true
                } ),
                $month = $( '#custom-month' ).html( cal.getMonthName() ),
                $year = $( '#custom-year' ).html( cal.getYear() );

        $( '#custom-next' ).on( 'click', function() {
                cal.gotoNextMonth( updateMonthYear );
        } );
        $( '#custom-prev' ).on( 'click', function() {
                cal.gotoPreviousMonth( updateMonthYear );
        } );

        function updateMonthYear() {				
                $month.html( cal.getMonthName() );
                $year.html( cal.getYear() );
        }

        // just an example..
        function showEvents( $contentEl, dateProperties ) {

                hideEvents();

                var $events = $( '<div id="custom-content-reveal" class="custom-content-reveal"><h4>Reuniones para hoy ' + dateProperties.monthname + ' ' + dateProperties.day + ', ' + dateProperties.year + '</h4></div>' ),
                        $close = $( '<span class="custom-content-close"></span>' ).on( 'click', hideEvents );

                $events.append( $contentEl.html() , $close ).insertAfter( $wrapper );

                setTimeout( function() {
                        $events.css( 'top', '0%' );
                }, 25 );

        }
        function hideEvents() {

                var $events = $( '#custom-content-reveal' );
                if( $events.length > 0 ) {

                        $events.css( 'top', '100%' );
                        Modernizr.csstransitions ? $events.on( transEndEventName, function() { $( this ).remove(); } ) : $events.remove();

                }

        }

};
</script>