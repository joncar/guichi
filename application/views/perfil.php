<div class="container">
    <div class="alert alert-danger" style="display:none;"></div>
    <div class="row">
        <form id="myform" class="form-horizontal" role="form" onsubmit="return val(this)">
  	<div class="col-xs-12 col-sm-2 col-sm-offset-2">
            <label for="foto_file" style="cursor:pointer">
                <?php if(empty($user->foto)): ?>
                <?= img('img/vacio.png','width:100%',TRUE,'id="foto_img"') ?>
                <?php else: ?>
                <?= img('files/'.$user->foto,'width:100%',TRUE,'id="foto_img"') ?>
                <?php endif ?>                
            </label>
                <?= img('img/loading.gif','width:100px; display:none;',TRUE,'id="loading"') ?>
                <input type="file" id="foto_file" name="foto_file" value="" style="display: none">
        </div>
        <div class="col-xs-12 col-sm-7" style="padding-top:20px;">
            
                <div>                
                <div class="row col-xs-12">
                Nombre: <?= form_input('nombre',$user->nombre,'class="ctrl" placeholder="Nombre"') ?> Apellidos: <?= form_input('apellido',$user->apellido,'class="ctrl" placeholder="apellido"') ?><br/>
                </div>
                <div class="row col-xs-12">
                Movil: <?= form_input('movil',$user->movil,'class="ctrl" placeholder="Movil"') ?> Email: <?= form_email('email',$user->email,'class="ctrl" placeholder="Email" style="width:45%"') ?><br/>
                </div>
                <div class="row col-xs-12">
                Sexo: <?= form_dropdown('sexo',array('0'=>'Masculino','1'=>'Femenino'),$user->sexo,'class="sexo" placeholder="Sexo"') ?><br/>
                </div>
                <div class="row">
                <div class="col-xs-2">Empresa: </div><div class="col-xs-10"><?= form_input('empresa',$user->empresa,'class="ctrl" placeholder="Empresa" style="width:100%"') ?></div>
                </div>
                <div class="row">
                    <div class="col-xs-2">Puesto: </div><div class="col-xs-10"><?= form_input('puesto',$user->puesto,'class="ctrl" placeholder="Puesto" style="width:100%"') ?></div>
                </div>
                </div>
                <input type="hidden" name="foto" value="<?= $user->foto ?>">
                <div style="margin-top:10px; text-align:right">
                    <a href="<?= base_url('changepassword') ?>" class="btn btn-default">Cambiar contraseña</a>
                    <button type="submit" id="enviar" class="btn btn-danger">Guardar</button>
                </div>            
        </div>  
        </form>
    </div>
    <div class="row col-xs-12" style="padding-top:20px;">
        <h2>Libreta de reuniones: <a href="<?= base_url('empresas/add') ?>" class="btn btn-danger"><i class="fa fa-plus"></i> Añadir</a></h2>
        <ul>
            <?php if($empresas->num_rows==0):?><li>Sin añadido</li><?php endif ?>
            <?php foreach($empresas->result() as $e): ?>
                <li class="row">
                    <div class="col-xs-10"><?= $e->nombre ?></div>
                    <div class="col-xs-2">
                        <a href='<?= base_url('empresas/edit/'.$e->id) ?>'><i class='fa fa-edit'></i></a>
                    </div>
                </li>
            <?php endforeach ?>
        </ul>
    </div>
    <div class="row col-xs-12" style="padding-top:20px;">
        <h2>Información de cuenta:</h2>
        
    </div>
    
</div>
<script>
    url_validation = '<?= base_url('usuarios/update_validation/'.$user->id) ?>';
    url = '<?= base_url('usuarios/update/'.$user->id) ?>';
    $(document).ready(function(){        
        $("#foto_file").change(function(){
            $("#loading").show();
            $("#foto_img").attr('src','');
            var formdata = new FormData(document.getElementById('myform'));
            $.ajax({
                url:'<?= base_url('upload_foto') ?>',
                contentType:false,
                processData:false,
                type:'post',
                data:formdata
            }).always(function(data){                
                $("#foto_img").attr('src',data);
                $("#loading").hide();
                $("#foto_file").val('');
                $("input[name='foto']").val(data);
            });
        });
        
        $(document).on('success_submit',function(){
            $("#enviar").html('Guardar');
        });
        $(document).on('error_submit',function(data,a){            
            $(".alert").html(a.error_message);
            $(".alert").show();
            $("#enviar").html('Guardar');
        })
    });
</script>