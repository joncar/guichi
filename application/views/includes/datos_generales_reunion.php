<h2>Datos generales</h2>
<div class="form-group">
    <div class="row">
        <div class="col-xs-12">
            Fecha: <input type="text" class="ctrl datetime-input" id="field-fecha" name='fecha' value="<?= empty($reunion)?date("d/m/Y"):date("d/m/Y",strtotime($reunion->fecha)) ?>">
            <?php 
              $hora = array();
              for($i=0;$i<2;$i++){
                  $ampm = $i==0?'AM':'PM';
                  for($k=1;$k<13;$k++){
                      $h = $ampm=='PM'?$k+12:$k;
                      $h = $k==12 && $ampm=='PM'?'00':$h;
                      $hora[$h.':00'] = $k.':00 '.$ampm;
                      $hora[$h.':30'] = $k.':30 '.$ampm;
                  }
              }
            ?>
            <?php 
                $horas = empty($reunion)?0:date("H:i",strtotime($reunion->hora));
                $horas2 = empty($reunion)?0:$reunion->duracion;
            ?>
            Hora: <?= form_dropdown('hora',$hora,$horas,'id="field-hora" class="ctrl"') ?>
            Hora Final: <?= form_dropdown('duracion',$hora,$horas2,'id="field-duracion" class="ctrl"') ?>
            <?php $libreta = array(''=>'Seleccione una libreta'); foreach($empresas->result() as $n=>$e): ?>
            <?php $libreta[$e->id] = $e->nombre; $sel = $n==0?$e->id:$sel; ?>
            <?php endforeach ?>
            <?php $sel = empty($reunion)?$sel:$reunion->libreta ?>
            Libreta: <?= form_dropdown('libreta',$libreta,$sel,'id="form-libreta" class="ctrl"') ?><br/>
        </div>
    </div>
  <div class="row">
      <div class="col-xs-12 col-sm-8">
          Lugar: <input type="text" class="ctrl" id="field-lugar" style='width:80%;' name='lugar' placeholder="Lugar de la reunión" value="<?=  empty($reunion)?'':$reunion->lugar ?>"><a href="javascript:mostrarMapa()"><i class="fa fa-map-marker"></i></a><br/>
        <input type="hidden" name="mapa" value="" id="mapainput">
      </div>
      <div class="col-xs-12 col-sm-8" id="mapacontent" style="display:none;">
          <div id="map" style="width:100%; height:200px"></div>
      </div>
  </div>
    <div class="row">
    	<div class="col-xs-12">
            Titulo: <input type="text" class="ctrl" id="field-titulo" style='width:80%;' name='titulo' placeholder="Titulo de la reunión" value="<?=  empty($reunion)?'':$reunion->titulo ?>"><br/>
            Objetivo: <textarea name="objetivo" class='ctrl' placeholder="Objetivo" style='width:80%'><?=  empty($reunion)?'':$reunion->objetivo ?></textarea>
        </div>
    </div>
</div>

<script>
var center = new google.maps.LatLng<?= empty($reunion)?'(17.0617633,-96.7340062)':$reunion->mapa ?>;
var mapOptions = {
    zoom: 15, 
    scrollwheel: false,
    zoomControl: true, 
    mapTypeControl: false, 
    scaleControl: false, 
    streetViewControl: false, 
    center: center
};
var mapElement = document.getElementById('map'); 
var map = new google.maps.Map(mapElement, mapOptions);
var mark = new google.maps.Marker({position: center, map: map, title:'Lugar de reunión',draggable:true,icon:'<?= base_url('img/mark.png') ?>'});
google.maps.event.addDomListener(mark,'dragend',function(e){    
    $("#mapainput").val('('+e.latLng.lat()+','+e.latLng.lng()+')');
    fillDirectionFromPoint(e.latLng.lat(),e.latLng.lng());
});
$("#mapainput").val('(17.0617633,-96.7340062)');
$("#field-lugar").on('change',function(){
   searchDirection($(this).val()); 
});

function searchDirection(street){
    var geocoder = new google.maps.Geocoder();
    var address = street;
    geocoder.geocode({'address': address}, function(results, status) {
        if (status === google.maps.GeocoderStatus.OK) {                
            map.setCenter(results[0].geometry.location);      
            mark.setPosition(results[0].geometry.location);
        }
    });
}

function fillDirectionFromPoint(lat,lon){
    var geocoder = new google.maps.Geocoder;
    geocoder.geocode({'latLng': new google.maps.LatLng(lat,lon)}, function(results, status) {
        //calle
        $("#field-lugar").val(results[0].formatted_address);
    });
}

function mostrarMapa(){
    if($("#mapacontent").hasClass('showed')){
        $("#mapacontent").hide().removeClass('showed');
    }else{
        $("#mapacontent").show().addClass('showed');
        google.maps.event.trigger(map, "resize");
    }
}
</script>