<div class="container" id='body'>
<div align="left" style="color:red"><h4>Reunión #<?= $reunion->id ?></h4></div>
<?php $this->load->view('includes/datos_generales_reunion'); ?>
<h2>Lista de asistentes</h2>
<table class="table invitados">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Inicial</th>
            <th>Puesto</th>
            <th style="text-align:center">Convocó</th>
            <th style="text-align:center">Lider</th>
            <th style="text-align:center">Confirm. Asistencia</th>
            <th style="text-align:center">Asistió</th>
        </tr>
    </thead>
    <tbody>        
        <?php $data = array('Selecciona un responsable') ?>
        <?php foreach($invitados->result() as $i): ?>
        <tr class='invitados_row'>
            <td><?= $i->nombre.' '.$i->apellido.' | '.$i->email ?></td>
            <td><?= $i->iniciales ?></td>
            <td><?= $i->puesto ?> </td>
            <td style="text-align:center"><?= $i->convoco==1?'<i class="label label-success">Si</i>':'<i class="label label-danger">No</i>' ?></td>
            <td style="text-align:center"><?= $i->lider==1?'<i class="label label-success">Si</i>':'<i class="label label-danger">No</i>' ?></td>            
            <td style="text-align:center"><?= $i->asiste==1?'<i class="label label-success">Si</i>':'<i class="label label-danger">No</i>' ?></td>            
            <td style="text-align:center"><input type="checkbox" data-val="<?= $i->id ?>" <?= $i->asistio==1?'checked':'' ?> class="asistio form-control"></td>            
            <?php $data[$i->iduser] = $i->iniciales; ?>
        </tr>
        <?php endforeach ?>       
    </tbody>
</table>
<div id="_reunion">
    <?php $this->load->view('includes/_reunion',array('data'=>$data)); ?>
</div>
