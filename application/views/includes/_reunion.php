<h2>Agenda <?php if ($reunion->status == 0 && $reunion->user == $_SESSION['user']): ?><a href="<?= base_url('reunion_update/' . $reunion->id . '/init') ?>" class="btn btn-info">Iniciar Reunión</a><?php endif ?></h2>
<div class="row invitadoss">
    <div class="row" style="font-weight:bold">
        <div class="col-xs-7">Temas y Tareas</div>
        <div class="col-xs-2">Responsable</div>
        <div class="col-xs-2">Adjunto</div>
    </div> 
    <form onsubmit="return enviaractividad(this)">    
        <?php foreach ($this->db->get_where('temas', array('reunion' => $reunion->id))->result() as $n => $t): ?>           
            <div>
                <div class="row ctrl">        
                    <div class="col-xs-7"><?= romanic_number($n + 1) . '. ' . $t->tema ?> 
                        <a href='#' title='Agregar texto correspondiente al desarollo normal del tema' class="add" data-val="normal" data-rel="<?= $t->id ?>" style="font-size:20px; color:green"><i class="glyphicon glyphicon-list-alt"></i></a> 
                        <a href='#' title='Agregar Nota importante' class="add"  data-val="important" data-rel="<?= $t->id ?>" style="font-size:20px; color:red"><i class="fa fa-exclamation-circle"></i></a> 
                        <a href='#' title='Agregar Idea Importante' class="add"  data-val="important2" data-rel="<?= $t->id ?>" style="font-size:20px; color:#66ccff"><i class="fa fa-lightbulb-o"></i></a> 
                        |
                        <a href='#' title='Agregar una Tarea' class="add" data-val="actividad" data-rel="<?= $t->id ?>" style="font-size:20px; color:green"><i class="fa fa-cog"></i></a>
                        <a href='#' title='Agregar una nota personal (solo será visible por ti)' class="<?= $reunion->user == $_SESSION['user'] && $reunion->status == 1 ? 'add' : '' ?> nota" data-val="nota" data-rel="<?= $t->id ?>" style="font-size:20px; color:green"><i class="fa fa-file-o"></i></a> </div>                        
                    <div class="col-xs-2">
					<?php
						$this->db->select('iniciales');    
						$this->db->from('temas_responsables');
						$this->db->join('invitados', 'temas_responsables.id_user = invitados.invitado');
						$this->db->where(array('id_tema' => $t->id, 'reunion' => $reunion->id));
						$query = $this->db->get();
								
						$arrayIniciales = array();
						foreach ($query->result() as $r){
							$arrayIniciales[] = $r->iniciales;
						}
						echo implode(" | ", $arrayIniciales);
						
					?></div>                    
                    <div class="col-xs-2"><?php if (!empty($t->adjunto)) { ?><a target="_new" href="<?= base_url('files/' . $t->adjunto); ?>"><?= $t->adjunto ?></a><?php } ?></div>                
                </div>
                <ol class="row lista" style="width:100%;">
                    <?php $this->db->order_by('priority', 'ASC'); ?>
                    <?php $this->db->where('reunion = ' . $reunion->id . ' AND tema = ' . $t->id . ' AND (user = 0 OR user = ' . $_SESSION['user'] . ')', null, FALSE); ?>
                    <?php foreach ($this->db->get_where('tareas', array('reunion' => $reunion->id, 'tema' => $t->id))->result() as $ta): ?>
                        <?php
                        switch ($ta->tipo) {
                            case 'normal':
                            case 'nota':
                                $width = '95%';
                                $placeholder = $ta->tipo == 'nota' ? 'Nota personal con respecto a este punto, solo visible para el usuario que la publica.' : 'Texto citado que corresponde al desarrollo normal del tema';
                                $col = '';
                                $col2 = '';
                                $col3 = '';
                                $icon = 'glyphicon glyphicon-list-alt';
                                if ($ta->tipo == 'nota')
                                    $icon = 'fa fa-file-o';
                            case 'actividad':
                                if ($ta->tipo == 'actividad') {
                                    $fecha = $ta->entrega != '0000-00-00' ? $ta->entrega : date("Y-m-d");
                                    $col = '<input type="number" data-rel="' . $ta->id . '" readonly name="progreso" value="0" class="form-control" style="width:60px; display:inline-block;">%';
                                    $col2 = form_dropdown('responsable', $data, $ta->responsable, 'class="form-control" data-rel="' . $ta->id . '"');
                                    $col.= ' Entrega: <input type="text" data-rel="' . $ta->id . '" value="' . date('d/m/Y', strtotime($fecha)) . '" name="entrega" class="form-control datetime-input" style="display:inline-block; width:140px" placeholder="dd/mm/yyyy">';
                                    $file = !empty($ta->adjunto) ? base_url('files/' . $ta->adjunto) : '';
                                    $file2 = !empty($ta->adjunto) ? '<i class="fa fa-download"></i>' : '';
                                    $col3 = '<label for="adjuntar_' . $ta->id . '"><i class="fa fa-upload"></i></label>' . img('img/loading.gif', 'width:40px; display:none;', TRUE, 'id="loading"') . '<input type="file" id="adjuntar_' . $ta->id . '" name="adjuntar" data-rel="' . $ta->id . '" style="display:none"> | <a href="' . $file . '" target="_new">' . $file2 . ' ' . $ta->adjunto . '</a>';
                                    $width = '50%';
                                    $placeholder = 'Texto citado que corresponde a una información importante en el desarrollo del tema';
                                    $icon = $ta->tipo == 'actividad' ? 'fa fa-cog' : $icon;
                                }
                                $color = 'green';
                                break;
                            case 'important':
                                $col = '';
                                $col2 = '';
                                $col3 = '';
                                $color = 'red';
                                $width = '95%';
                                $icon = 'fa fa-exclamation-circle';
                                $placeholder = 'Texto citado que corresponde a una idea importante en el desarrollo del tema';
                                break;
                            case 'important2':
                                $col = '';
                                $col2 = '';
                                $col3 = '';
                                $color = '#66ccff';
                                $width = '95%';
                                $icon = 'fa fa-lightbulb-o';
                                $placeholder = 'Texto citado que corresponde a una idea importante en el desarrollo del tema';
                                break;
                        }
                        ?>
                        <li style="font-weight:bold" data-rel="<?= $ta->id ?>">
                            <div style="margin-top: -18px; margin-left: 10px;" class="row"><div class="col-xs-7">
                                    <i style="color:<?= $color ?>; cursor:move; vertical-align: top;" class="<?= $icon ?>"></i> <textarea data-rel = '<?= $ta->id ?>' name="nombre" style="height:40px; display:inline-block; width:<?= $width ?>" class="form-control <?= $ta->tipo == 'nota' ? 'notat' : '' ?>" placeholder="<?= $placeholder ?>"><?= str_replace("<br/>", "\n", $ta->nombre) ?></textarea>
        <?= $col ?>
                                </div>
                                <div class="col-xs-2"><?= $col2 ?></div>
                                <div class="col-xs-2"><?= $col3 ?></div>
                                <div class="col-xs-1"><a style="color:red" data-rel="<?= $ta->id ?>" href="#" class="remove <?= $ta->tipo == 'nota' ? 'removenota' : '' ?>"><i class="glyphicon glyphicon-remove"></i></a></div>
                            </div></li>
            <?php endforeach ?>
                </ol>
            </div>    
<?php endforeach ?>
    </form>
</div>
<h2>Notas Generales </h2>
<textarea name="nota" class="form-control"><?= $reunion->nota ?></textarea>
<?php if (!empty($msj)): ?>
    <div class="alert alert-success">Reunión Cerrada con éxito</div>
    <?php endif ?>
<div style="margin:20px"><?php if ($reunion->user == $_SESSION['user']) { ?> 
        <a href='<?= base_url('reprogramar/' . $reunion->id . '/new') ?>' class='btn btn-success'>Programar Reunión de seguimiento</a> 
        <?php if ($reunion->status == 1) { ?>
            <a href='<?= base_url('reunion_update/' . $reunion->id . '/close') ?>' class='btn btn-danger'>Cerrar reunión</a> 
        <?php } ?>
    <?php } ?>
<?php if ($reunion->status == 2) { ?>
        <a href='<?= base_url('panel/imprimir_reporte/1/' . $reunion->id) ?>' class='btn btn-default'><i class='fa fa-print'></i> Imprimir Reunión</a> 
    <?php } ?>
</div>
</div>
<div id="responsableField" style="display:none">
<?= form_dropdown('responsable', $data, '', 'class="form-control"') ?>
</div>