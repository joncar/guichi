<header><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    <div class="row" id="banner">    
        <div class="col-xs-12 col-sm-8"><?= transform_day(date("w")).', a '.date('d').' de '.meses(date("m")).' de '.date("Y").' | Hora: '.date("H:i") ?></div>
        <div class="col-xs-12 col-sm-4" align="right">
        <?= !empty($_SESSION['user'])?
            '<a href="'.base_url('panel').'">'.img($this->querys->get_foto(),'width:20px').' '.$_SESSION['nombre'].' '.$_SESSION['apellido'].'</a> | Cuenta: <a href="'.base_url('upgrade').'">'.$this->querys->get_membresia().'</a>':'Visitante' ?>
        <?php if(!empty($_SESSION['user']) && $_SESSION['cuenta']==1): ?>| <a href="<?= base_url('admin') ?> ">Admin</a><?php endif ?>
        <?php if(!empty($_SESSION['user']))echo '| <a href="'.base_url('main/unlog').'">Salir</a>'; ?>
        </div>
    </div>
    <div id="subbaner" class="row">        
        <div class="col-xs-6 col-sm-2"><a href="<?= site_url() ?>"><?= img('img/logo.png','width:100%') ?></a></div>
        <?php if(!empty($_SESSION['user'])): ?>
        <div style="margin-top:20px;" class="col-xs-6 col-sm-3 col-sm-offset-2" align="center"><a href='<?= base_url('reunionadd') ?>' class="btn btn-success btn-lg"><i class="fa fa-plus"></i> Nueva Reunión</a></div>
        <div class="col-xs-11 col-sm-5 row header-botones">
            
            <div style="display: inline-block; width:77px;">
                <div class="header-botones-link"><a href="<?= base_url('calendario') ?>"><i class="fa fa-calendar"></i></a></div>
                <div>Mi calendario</div>
            </div>

            <div style="display: inline-block; width:77px;">
                <div class="header-botones-link"><a href="<?= base_url('reuniones') ?>"><i class="fa fa-group"></i></a></div>
                <div>Mis reuniones</div>
            </div>
            <div style="display: inline-block; width:77px;">
                <div class="header-botones-link"><a href="<?= base_url('tareas') ?>"><i class="fa fa-list"></i></a></div>
                <div>Mis tareas</div>
            </div>
             <div style="display: inline-block; width:77px;">
                <div class="header-botones-link"><a href="<?= base_url('contactos') ?>"><i class="fa fa-book"></i></a></div>
                <div>Mis contactos</div>
            </div>            
            <div style="display: inline-block; width:77px;">
                <div class="header-botones-link"><a href="<?= base_url('perfil') ?>"><i class="fa fa-user"></i></a></div>
                <div>Mi perfil</div>
            </div>
        </div>      
        <?php endif ?>
    </div>
</header>