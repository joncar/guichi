<div class="container">    
    <?php if(!empty($reunion) && $reunion->status>1): ?>        
    <div class="alert alert-danger">Esta reunión solo está visible para consulta ya que no puede ser editada.</div>
    <?php endif ?>
    <div class="row">   
        <form role="form" onsubmit="return val(this)">
        	<?php 
			$id_reunion_anterior = 0;
			if(isset($new) && !empty($reunion)){
				$id_reunion_anterior = $reunion->id;				
			}else{
				if(!empty($reunion) && $reunion->id_reunion_anterior > 0){
					$id_reunion_anterior = $reunion->id_reunion_anterior;
				}
			}
			?> 
        	<input type="hidden" name="id_reunion_anterior" value="<?= $id_reunion_anterior ?>" />
            <?php 
			$data = array(''=>'Seleccione un contacto'); 
			foreach($contactos->result() as $c):				
            	$data[$c->id] = $c->nombre.' '.$c->apellido.' | '.$c->email;
            endforeach			
			?>
            
            <?= $this->load->view('includes/datos_generales_reunion',array('data'=>$data)) ?>
            <h2>Lista de asistentes 
                <a href="javascript:addinvitado()" class="btn btn-danger"><i class="fa fa-plus"></i></a> 
                <a onclick="ventana = window.open('<?= base_url('contactos/popup/add/1') ?>','_blank','height=560,width=420'); 
                ventana.onload = function(){
                	ventana.scrollTo(0, ventana.document.body.scrollHeight);
                }" class="btn btn-success"><i class="fa fa-plus"></i><i class="fa fa-user"></i></a>
			</h2>
            <table class="table invitados">
                <thead>
                    <tr>
                        <th>Nombre</th><th>Inicial</th><th>Rol</th><th>Convocó</th><th>Lider</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(empty($reunion)): ?>
                    <tr class='invitados_row'>
                        <td><?= form_dropdown('invitado_contacto[]',$data,$_SESSION['user'],'id="field-invitado_contacto" class="form-control invitado_contacto"'); ?></td>
                        <td style="width:10%"><input type="text" name="inicial[]" id="field-inicial" placeholder="Inicial" class="form-control" value='<?= $this->querys->get_iniciales($user) ?>'></td>
                        <td><input type="text" name="puesto[]" id="field-puesto" placeholder="Puesto" class="form-control" value='<?= $user->puesto ?>'></td>
                        <td style="width:1%"><input type="checkbox" name="convoco[]" id="field-convoco" placeholder="Convocó" checked class="form-control"></td>
                        <td style="width:1%"><input type="checkbox" name="lider[]" id="field-lider" placeholder="Lider" checked class="form-control"></td>
                        <td style="width:1%">
                            <div style="cursor:pointer; display:none" class='minus delete_invitado btn btn-danger btn-sm'>
                                <i class='fa fa-minus'></i>
                            </div>
                        </td>
                    </tr>
                    <?php else: ?>
                    <?php foreach($invitados->result() as $n=>$i): ?>
                    <tr class='invitados_row'>
                        <td><?= form_dropdown('invitado_contacto[]',$data,$i->invitado,'id="field-invitado_contacto" class="form-control invitado_contacto"'); ?></td>
                        <td style="width:10%"><input type="text" name="inicial[]" id="field-inicial" placeholder="Inicial" class="form-control"  value='<?= $i->iniciales ?>' ></td>
                        <td><input type="text" name="puesto[]" id="field-puesto" placeholder="Puesto" class="form-control" value='<?= $i->puesto ?>'></td>
                        <td style="width:1%"><input type="checkbox" name="convoco[]" value='1' <?= $i->convoco==1?'checked':'' ?> id="field-convoco" placeholder="Convoco" class="form-control"></td>
                        <td style="width:1%"><input type="checkbox" name="lider[]" value='1' <?= $i->lider==1?'checked':'' ?> id="field-lider" placeholder="Lider" class="form-control"> </td>
                        <td style="width:1%">
                            <div style="cursor:pointer; <?= $n==0?'display:none':'' ?>" class='minus delete_invitado btn btn-danger btn-sm'>
                                <i class='fa fa-minus'></i>
                            </div>
                        </td>
                    </tr>
                    <?php endforeach ?>
                    <?php endif ?>                    
                    <tr class='invitados_row'>
                        <td><?= form_dropdown('invitado_contacto[]',$data,'','id="field-invitado_contacto" class="form-control invitado_contacto"'); ?></td>
                        <td style="width:10%"><input type="text" name="inicial[]" id="field-inicial" placeholder="Inicial" class="form-control" value='' ></td>
                        <td><input type="text" name="puesto[]" id="field-puesto" placeholder="Puesto" class="form-control" value=''></td>
                        <td style="width:1%"><input type="checkbox" name="convoco[]" value='1' id="field-convoco" placeholder="Convoco" class="form-control"></td>
                        <td style="width:1%"><input type="checkbox" name="lider[]" value='1' id="field-lider" placeholder="Lider" class="form-control"> </td>
                        <td style="width:1%">
                            <div style="cursor:pointer;" class='minus delete_invitado btn btn-danger btn-sm'>
                                <i class='fa fa-minus'></i>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <h2>Agenda <a href="javascript:addtema()" class="btn btn-danger"><i class="fa fa-plus"></i></a></h2>            
            <table class="table">
                <thead>
                    <tr>
                        <th>Temas</th><th>Responsable</th><th>&nbsp;</th><th>Adjuntar</th><th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody class="tema">
                    <?php if(!empty($reunion)): foreach($temas->result() as $n=>$t): ?>
                    <tr class="tema_row">
                        <td class="col-xs-6"><input type="text" name="tema[]" id="field-tema" placeholder="Tema" value="<?= $t->tema ?>" class="form-control"></td>
                        <td class="col-xs-4">
                        <?php
							$this->db->select('id_user');    
							$this->db->from('temas_responsables');
							$this->db->join('invitados', 'temas_responsables.id_user = invitados.invitado');
							$this->db->where(array('id_tema' => $t->id, 'reunion' => $reunion->id));
							$query = $this->db->get();
							
							foreach ($query->result() as $r){
								echo form_dropdown('responsable['.$n.'][]',$data,$r->id_user,'id="field-responsable" class="form-control invitacion_responsable" nro="'.$n.'"');
							}
						?>
                        </td>
                        <td class="col-xs-1" style="white-space:nowrap;">
                            <a onclick="agregarResponsable(this);" class="btn btn-success btn-sm"><i class="fa fa-plus"></i><i class="fa fa-user"></i></a>
                            <a onclick="eliminarResponsable(this);" class="btn btn-danger btn-sm"><i class="fa fa-minus"></i><i class="fa fa-user"></i></a>
                        </td>
                        <td class="col-xs-2">
                            <input type="file" name="adjuntar[]" id="field-adjuntar">
                            <?php if(!empty($reunion) && !isset($new)): ?><input type="hidden" name="adjunto[]" value="<?= $t->adjunto ?>"><?php endif ?>
                        </td>
                        <td class="col-xs-2">
                            <div style="cursor:pointer; <?= $n==0?'display:none':'' ?>" class='minus delete_tema btn btn-danger btn-sm'>
                                <i class='fa fa-minus'></i>
                            </div>
                        </td>
                    </tr>
                    <?php endforeach;?>
                    <?php else: ?>
                        <tr class="tema_row">
                            <td class="col-xs-6"><input type="text" name="tema[]" id="field-tema" placeholder="Tema" class="form-control"></td>
                            <td class="col-xs-4" style="white-space:nowrap;"><?= form_dropdown('responsable[0][]',$data,'','id="field-responsable" class="form-control invitacion_responsable" nro="0"'); ?></td>
                            <td class="col-xs-1" style="white-space:nowrap;">
                            	<a onclick="agregarResponsable(this);" class="btn btn-success btn-sm"><i class="fa fa-plus"></i><i class="fa fa-user"></i></a>
                                <a onclick="eliminarResponsable(this);" class="btn btn-danger btn-sm"><i class="fa fa-minus"></i><i class="fa fa-user"></i></a>
                            </td>
                            <td class="col-xs-2"><input type="file" name="adjuntar[]" id="field-adjuntar"></td>
                            <td class="col-xs-2">
                                <div style="cursor:pointer; display:none" class='minus delete_tema btn btn-danger btn-sm'>
                                    <i class='fa fa-minus'></i>
                                </div>
                            </td>
                        </tr>
                    <?php endif ?>
                </tbody>
            </table>
            <h2>Nota de la invitación</h2>
            <textarea class="ctrl form-control" name="nota" id="nota" placeholder="Escribe una nota para tus invitados"><?= empty($reunion)?'':$reunion->nota ?></textarea>
            <?php if(empty($reunion) || !empty($reunion) && $reunion->status<=1): ?>        
                <div style="margin:40px; text-align:center">
                    <div class="alert alert-danger" style="display:none;"></div>
                    <button id="btnEnviarInvitaciones" type="submit" class="btn btn-success btn-lg" onclick="this.disabled = true;">Enviar invitaciones</button>
                    <?= img('img/loading.gif','width:100px; display:none;',TRUE,'id="loading"') ?>
                </div>
            <?php endif ?>
        </form>
    </div> 
</div>

<script>
    var contactos = [];
    url_validation = '<?= base_url('reunion_validation') ?>';
    url = '<?= empty($reunion) || isset($new)?base_url('reunion_add'):base_url('reunion_edit/'.$reunion->id) ?>';
    var new_user = undefined;
    <?php foreach($contactos->result() as $c): ?>
        <?php echo 'contactos.push({id:'.$c->id.',puesto:\''.$c->puesto.'\',iniciales:\''.$c->iniciales.'\'});' ?>
    <?php endforeach ?>
    
    function addinvitado(){
        var tr = $(".invitados").find('.invitados_row')[0];
        $(".invitados").append($(tr).clone());
        row = $(".invitados_row").last();
        row.find('input').val('');
        row.find('select').val(''); 
        row.find('.minus').show();
        row.find('input[type="checkbox"]').prop('checked',false);
        evento();
    }
    function addtema(){
        var tr = $(".tema").find('.tema_row')[0];		
        $(".tema").append($(tr).clone());
        var row = $(".tema .tema_row").last();
		row.find('.minus').show();
        row.find('input').val('');		
        row.find('select').val('');		
		row.find('select').not(":first").remove();
        evento();
		
		actSelectResponsable();
    }
    
    function refresh_new_users(){
        $.post('<?= base_url('get_last_contacts') ?>',{},function(data){
            data = JSON.parse(data);
            new_user = data.options;
            contactos = data.iniciales;
            $(".invitado_contacto").each(function(){
                valor = $(this).val();
                $(this).html(new_user);
                $(this).val(valor);
            });
        });
    }
    $(document).on('change',".invitado_contacto",function(){        
        for(i in contactos)
        {
            if(contactos[i].id==$(this).val())
            {
                $(this).parents('tr').find('#field-inicial').val(contactos[i].iniciales);
                $(this).parents('tr').find('#field-puesto').val(contactos[i].puesto);                     
            }
        }
    });
        
    refresh_new_users();
    function evento(){
        $(".delete_invitado").click(function(){
            $(this).parent('td').parent('tr').remove();
			actualizarResponsable();
        });

        $(".delete_tema").click(function(){
            $(this).parent('td').parent('tr').remove();
			actSelectResponsable();
        });
                

        $(".datetime-input").change(function(){
            var str = $(this).val();
            str = str.split(' ');
            $(this).val(str[0]);
            $("#field-hora").val(str[1]);
        });

        $('input, textarea').keypress(function(event){
            var enterOkClass =  $(this).attr('class');
            if (event.which == 13 && enterOkClass != 'enterSubmit') {
                event.preventDefault();
                return false;   
            }
        });                
    }

    $(document).on('error_submit',function(data,a){            
        $(".alert").html(a.error_message);
        $(".alert").show();
        $("#enviar").html('Guardar');
    })

    $(document).ready(function(){        
        $(document).on('success_submit',function(data,a){
            if(a.success){
                $(".alert").addClass('alert-success');
                $(".alert").removeClass('alert-danger');
                $(".alert").show();
                $(".alert").html('Reunion creada con exito.');
            }
            else{                
                $(".alert").html(a.error_message);
                $(".alert").show();
                $(".alert").removeClass('alert-success');
                $(".alert").addClass('alert-danger');
                $("#enviar").html('Guardar');
            }
        });

        $(document).on('error_submit',function(data,a){            
            $(".alert").html(a.error_message);
            $(".alert").show();
            $(".alert").removeClass('alert-success');
            $(".alert").addClass('alert-danger');
            $("#enviar").html('Guardar');
        });

        evento();

        $(document).on('change','.invitado_contacto',function(){
			actualizarResponsable();
        });
    });
	
	function actualizarResponsable(){
		var arraySeleccionados = []; 
		$(".invitacion_responsable option:selected").each(function(){
			arraySeleccionados.push($(this).val());
		});
		
		$(".invitacion_responsable").html('');
		$(".invitacion_responsable").append('<option value="">Seleccione un contacto</option>');
		$(".invitado_contacto").each(function(index){
			$(".invitacion_responsable").append('<option value="'+$(this).val()+'">'+$(this).find('option:selected').html()+'</option>');
		});
		
		$(".invitacion_responsable").each(function(index, objSelect) {
			$(objSelect).val(arraySeleccionados[index]);
		});
	}
	
	function agregarResponsable(obj){
		var lista = $(obj).closest("tr").find("#field-responsable");		
		var td = lista.closest("td");
		td.append($(lista[0]).clone().val(''));
	}
	
	function eliminarResponsable(obj){
		$(obj).closest("tr").find("#field-responsable:not(:first-child)").last().remove();
	}
	
	function actSelectResponsable(){
		var obj = $(".tema_row");
		$.each(obj, function(key, objTr){
			$.each($(objTr).find("#field-responsable"), function(indice, objSelect){
				$(objSelect).attr("name", "responsable["+key+"][]");
				$(objSelect).attr("nro", key);
			});
		});
	}
</script>