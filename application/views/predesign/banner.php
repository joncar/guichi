<?php $banner = $this->db->get('banner') ?>
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->    
    <ol class="carousel-indicators">
        <?php foreach($banner->result() as $n=>$b): ?>
        <li data-target="#carousel-example-generic" data-slide-to="<?= $n ?>" class="<?= $n==0?'active':'' ?>"></li>        
        <?php endforeach ?>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <?php foreach($banner->result() as $n=>$b): ?>
        <div class="item <?= $n==0?'active':'' ?>">
           <?= img('img/'.$b->imagen,'width:100%; visibility:hidden') ?>
        </div>        
        <?php endforeach ?>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
</div>