<!Doctype html>
<html lang="es">
	<head>
		<title><?= empty($title)?'Guichi.com.mx':$title ?></title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<?php 
		if(!empty($css_files) && !empty($js_files)):
		foreach($css_files as $file): ?>
		<link type="text/css" rel="stylesheet" href="<?= $file ?>" />
		<?php endforeach; ?>
		<?php foreach($js_files as $file): ?>
		<script src="<?= $file ?>"></script>
		<?php endforeach; ?>                
                <?php endif; ?>                                
                <?php if(empty($crud)): ?>
                    <script src="http://code.jquery.com/jquery-1.9.0.js"></script>
                    <script src="<?= base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
                <?php endif ?>
		<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBAmpz-tft5eyIVdHRZrXcEhj4ykyaC80E&sensor=true"></script>                
                <script src="<?= base_url('js/file_upload.js') ?>"></script>
		<script src="<?= base_url('assets/frame.js') ?>"></script>
                <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
                <link rel="stylesheet" type="text/css" href="<?= base_url('assets/bootstrap/css/bootstrap.css') ?>">
                <link rel="stylesheet" type="text/css" href="<?= base_url('css/font-awesome.css') ?>">
                <link rel="stylesheet" type="text/css" href="<?= base_url('css/style.css') ?>">	
                <!-- Calendario -->
                <link rel="stylesheet" type="text/css" href="<?= base_url('css/calendar.css') ?>" />
		<link rel="stylesheet" type="text/css" href="<?= base_url('css/custom_2.css') ?>" />
		<script src="<?= base_url('js/modernizr.custom.63321.js') ?>"></script>
                <script src="<?= base_url('js/frame.js') ?>"></script>                
                <script>
                    var url_validation, url;
                </script>
	</head>
	<body>                          
            <section>
                <?= $this->load->view('includes/header'); ?>
                <article><?= $this->load->view($view); ?></article>
            </section>
            <footer>
                Guichi - 2014
            </footer>
<script>     
    function val(form)
    {        
            $(".alert").hide();
            $("#enviar").html('Guardando');
            $("#loading").show();
            var formdata = new FormData(form);
            $.ajax({
                url:url_validation,
                contentType:false,
                processData:false,
                type:'post',                
                data:formdata,
				error: function( data, status, error ) { 
					console.log("Error Ajax1");
					console.log(data);
					console.log(status);
					console.log(error);
				}
            }).always(function(data){
                data = data.replace('<textarea>','');
                data = data.replace('</textarea>','');
                data = JSON.parse(data);
                if(data.success){
                    $.ajax({
                        url:url,
                        contentType:false,
                        processData:false,
                        type:'post',                
                        data:formdata,
						error: function( data, status, error ) { 
							console.log("Error Ajax2");
							console.log(data);
							console.log(status);
							console.log(error);
						}
                    }).always(function(data){
                        data = data.replace('<textarea>','');
                        data = data.replace('</textarea>','');
                        data = JSON.parse(data);
                        $(document).trigger('success_submit',[data]);
                        $("#loading").hide();
												
						setTimeout(function(){//redirigo al panel principal
							window.location = '<?php echo base_url('panel/'); ?>';
						}, 2000);						
                    });
                }
                else{
					$("#btnEnviarInvitaciones").prop( "disabled", false );//para evitar quede doble click y guarde multiples
                    $(document).trigger('error_submit',[data]);
                    $("#loading").hide();
                }                
            });
        return false;
    }        
</script>
<?php $this->load->view('predesign/datepicker') ?>
        </body>
</html>