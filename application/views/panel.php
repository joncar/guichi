<section class="row" style="margin-right: 0px; margin-right:0px;">    
    <div class="col-sm-12 col-xs-12" style="padding:20px;">
        <?php if(!empty($crud)):$this->load->view('cruds/'.$crud); ?>
        <?php else: ?>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Lista de tus próximas reuniones
                  </a>
                </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <?php $this->db->order_by('fecha','ASC'); //$this->db->limit(5); ?>
                    <?php $this->db->select('reuniones.*'); $this->db->join('invitados','invitados.reunion = reuniones.id'); ?>
                    <?php foreach($this->db->get_where('reuniones',array('TIMESTAMP(reuniones.fecha, reuniones.hora)  >= '=>date("Y-m-d"." ".date("H:i:s")),'invitados.invitado'=>$_SESSION['user']))->result() as $r): ?>
                    
                        <?php $this->db->select('CONCAT(user.nombre," ",user.apellido) as nom',FALSE); $this->db->join('user','user.id = invitados.invitado'); $convoco = $this->db->get_where('invitados',array('reunion'=>$r->id))->row()->nom ?>
                        <div class='alert alert-info'>
							<?= '<b>'.$r->titulo.'</b> en <b>'.date("d/m/Y",strtotime($r->fecha)).'</b> en el lugar de <b>'.$r->lugar.'</b> a las <b>'.$r->hora.'</b> convocada por <b>'.$convoco.'</b>' ?>
                            <br><a href="<?php echo base_url("/reunion/".$r->id); ?>">Reunión <i class="fa fa-book"></i></a>
                        </div>
                        
                    <?php endforeach ?>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    Tus Tareas Pendientes
                  </a>
                </h4>
              </div>
              <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">  
                    <?php $this->db->order_by('entrega','ASC'); //$this->db->limit(5); ?>
                    <?php foreach($this->db->get_where('tareas',array('tareas.progreso < '=>100,'responsable'=>$_SESSION['user']))->result() as $r): ?>                        
                        <div class='alert alert-info'>
                        	<?= ($r->entrega != '' && $r->entrega != '0000-00-00') ? "Fecha de Entrega ".$r->entrega."<br>" : ""; ?> 
                            
							<?= '<b>'.$r->nombre.'</b>'; ?>
                            <br><a href="<?php echo base_url("/reunion/".$r->reunion); ?>">Reunión <i class="fa fa-book"></i></a>
                            
                            
                        </div>
                    <?php endforeach ?>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    Tareas que asignaste a otras personas
                  </a>
                </h4>
              </div>
              <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">
                  <?php $this->db->order_by('entrega','ASC'); //$this->db->limit(5); ?>
                  <?php 
                    $this->db->select('tareas.*, tareas.nombre as tarea, temas.tema as tem, concat(user.nombre," ",user.apellido) as resp',FALSE);
                    $this->db->join('invitados','invitados.reunion = reuniones.id');
                    $this->db->join('tareas','tareas.reunion = reuniones.id');
                    $this->db->join('user','user.id = tareas.responsable');
                    $this->db->join('temas','temas.id = tareas.tema');
                    $this->db->where('reuniones.user = '.$_SESSION['user'].' AND tareas.responsable != '.$_SESSION['user'].' OR reuniones.user != '.$_SESSION['user'].' AND tareas.responsable != '.$_SESSION['user'],'',FALSE);                    
                    $this->db->group_by('reuniones.id, tareas.responsable');
                    foreach($result = $this->db->get('reuniones')->result() as $r):?>
                        <div class='alert alert-info'>
                        	<?= ($r->entrega != '' && $r->entrega != '0000-00-00') ? "Fecha de Entrega ".$r->entrega."<br>" : ""; ?> 
							<?= 'Tarea <b>'.$r->tarea.'</b> de '.$r->resp; ?>
                            <br><a href="<?php echo base_url("/reunion/".$r->reunion); ?>">Reunión <i class="fa fa-book"></i></a>
                        </div>
                    <?php endforeach ?>
                </div>
              </div>
            </div>
          </div>
        <?php endif ?>
    </div>
</section>