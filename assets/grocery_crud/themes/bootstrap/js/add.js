var datos;
var form;
var primary;
var back;
var save_and_close = false;
$(document).ready(function(){
    $('#save-and-go-back-button').click(function(){
        save_and_close = true;
        $('#crudForm').trigger('submit');
    });
    
    $("#cancel-button").click(function(){
        if( confirm( message_alert_add_form ) )
        {
                window.location = list_url;
        }

        return false;
    })
})
function sendform(form)
{
    form = $(form);    
    datos = new FormData(document.getElementById(form.attr('id')));    
        $("textarea.texteditor").each(function(){
            datos.append($(this).attr('name'),$(this).val());
        })                
    $(".alert").hide();
    $.ajax({
        url: validation_url,
        data: datos,
        context: document.body,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST'
    })
    .always(
        function(data){
            data = data.replace("<textarea>","");
            data = data.replace("</textarea>","");
            data = JSON.parse(data);
            if(data.success){
                $.ajax({
                    url: form.attr('action'),
                    data: datos,
                    context: document.body,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST'
                })
                .always(
                    function(data){
                        data = data.replace("<textarea>","");
                        data = data.replace("</textarea>","");
                        data = JSON.parse(data);
                        if(data.success){
                            $(".alert-success").html(data.success_message);
                            $(".alert-success").show();
                            form.trigger('reset');
                            if(save_and_close)
                                window.location = data.success_list_url;
                        }
                        else
                        {
                            $(".alert-danger").html(data.error_message);
                            $(".alert-danger").show();
                        }
                    });
            }
            else
            {
                $(".alert-danger").html(data.error_message);
                $(".alert-danger").show();
            }
        });

    return false;
}