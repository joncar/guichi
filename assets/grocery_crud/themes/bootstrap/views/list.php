<div class="col-xs-12" style="overflow-x:auto">    
    <table class="table table-bordered table-hover" id="flex1" style="max-width: 100%;">
        <thead>
            <tr>
                <?php foreach($columns as $column): ?>
                <th>
                    <div class="<?php if(isset($order_by[0]) &&  $column->field_name == $order_by[0]){?><?php echo $order_by[1]?><?php }?>" rel='<?php echo $column->field_name?>'>
                            <?php echo $column->display_as?>
                    </div>
                </th>
                <?php endforeach ?>
                <?php if(!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)):?>
                <th align="left" abbr="tools" axis="col1" class="" width='20%'>
                        <div class="text-right">
                                <?php echo $this->l('list_actions'); ?>
                        </div>
                </th>
                <?php endif ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach($list as $num_row => $row): ?>        
                <tr>
                    <?php foreach($columns as $column){?>
                    <td width='<?php echo $column_width?>%' class='<?php if(isset($order_by[0]) &&  $column->field_name == $order_by[0]){?>sorted<?php }?>'>
                            <div class='text-left'><?php echo $row->{$column->field_name} != '' ? $row->{$column->field_name} : '&nbsp;' ; ?></div>
                    </td>
                    <?php }?>
                    <?php if(!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)): ?>
                    <td align="right" width='20%'>                            
                            <?php if(!$unset_read){?>
                                    <a href='<?php echo $row->read_url?>' title='<?php echo $this->l('list_view')?> <?php echo $subject?>' class="edit_button"><span class='fa fa-search'></span></a>
                            <?php }?>
                            <?php if(!$unset_edit){?>
                                    <a href='<?php echo $row->edit_url?>' title='<?php echo $this->l('list_edit')?> <?php echo $subject?>' class="edit_button"><span class='fa fa-edit'></span></a>
                            <?php }?>                            
                            <?php if(!$unset_delete){?>
                                <a href='<?php echo $row->delete_url?>' title='<?php echo $this->l('list_delete')?> <?php echo $subject?>'>
                                    <span style="color:red" class='fa fa-close'></span>
                                </a>
                            <?php }?>                            
                            <?php 
                            if(!empty($row->action_urls)){
                                    foreach($row->action_urls as $action_unique_id => $action_url){ 
                                            $action = $actions[$action_unique_id];
                            ?>
                                            <a href="<?php echo $action_url; ?>" class="<?php echo $action->css_class; ?> crud-action" title="<?php echo $action->label?>"><?php 
                                                    if(!empty($action->image_url))
                                                    {
                                                            ?><img src="<?php echo $action->image_url; ?>" alt="<?php echo $action->label?>" /><?php 	
                                                    }
                                            ?></a>		
                            <?php }
                            }
                            ?>
                    </td>
                        <?php endif ?>
                </tr>
            <?php endforeach ?> 
        </tbody>
    </table>
    <?php if(empty($list)): ?>
        <?= $this->l('list_no_items') ?>
    <?php endif ?>
</div>