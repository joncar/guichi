<?php
	$this->set_css($this->default_theme_path.'/bootstrap/css/bootstrap.css');
	$this->set_js_lib($this->default_javascript_path.'/'.grocery_CRUD::JQUERY);

	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
	$this->set_js_lib($this->default_javascript_path.'/common/lazyload-min.js');
	if (!$this->is_IE7()) {
		$this->set_js_lib($this->default_javascript_path.'/common/list.js');
	}
	$this->set_js($this->default_theme_path.'/bootstrap/js/cookies.js');		
	$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.numeric.min.js');	
	/** Fancybox */
	$this->set_css($this->default_css_path.'/jquery_plugins/fancybox/jquery.fancybox.css');
	$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.fancybox-1.3.4.js');
	$this->set_js($this->default_javascript_path.'/jquery_plugins/jquery.easing-1.3.pack.js');

	/** Jquery UI */
	$this->load_js_jqueryui();

?>
<script type='text/javascript'>
	var base_url = '<?php echo base_url();?>';

	var subject = '<?php echo $subject?>';
	var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
	var unique_hash = '<?php echo $unique_hash; ?>';

	var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";

</script>
<div id='list-report-error' class='alert alert-danger' style="display:none"></div>
<div id='list-report-success' class='alert alert-success' style="<?= empty($success_message)?'display:none':'' ?>">
<?php if($success_message !== null){?>
    <p><?php echo $success_message; ?></p>
<?php } ?>
</div>
<div>
    <?php if(!$unset_add || !$unset_export || !$unset_print):?>
    <div class="titulo row">
        <?php if(!$unset_add): ?>
        <div class="col-xs-6 col-sm-8"><a href='<?php echo $add_url?>' title='<?php echo $this->l('list_add'); ?> <?php echo $subject?>' class='btn btn-success btn-xs'><i class="fa fa-plus"></i> <?php echo $this->l('list_add'); ?> <?php echo $subject?></a></div>
        <?php endif ?>
        <?php if(!$unset_export || !$unset_print): ?>
        <div class="col-xs-6 col-sm-4" align="right">
            <?php if(!$unset_export): ?>
                <a href="<?php echo $export_url; ?>" target="_blank" style="margin:0 20px;">
                    <i class="fa fa-download"></i> <?php echo $this->l('list_export');?>                    
                </a>
            <?php endif ?>
            <?php if(!$unset_print): ?>
                <a href="<?php echo $print_url; ?>" target="_blank" style="margin:0 20px;">
                    <i class="fa fa-print"></i> <?php echo $this->l('list_print');?>                    
                </a>
            <?php endif ?>
        <?php endif ?>
        </div>
    </div>
    <?php endif ?>
    <div id='ajax_list' class="ajax_list row">
	<?php echo $list_view?>
    </div>
</div>    