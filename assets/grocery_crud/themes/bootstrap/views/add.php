<?php
        $this->set_css($this->default_theme_path.'/bootstrap/css/bootstrap.css');
	$this->set_js_lib($this->default_theme_path.'/bootstrap/js/add.js');
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
?>
<script>
	var validation_url = '<?php echo base_url('registro/index/insert_validation')?>';
	var list_url = '<?php echo $list_url?>';

	var message_alert_add_form = "<?php echo $this->l('alert_add_form')?>";
	var message_insert_error = "<?php echo $this->l('insert_error')?>";
</script>

<div class="row">
<div style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
    <!--<div class="titulo"><?php echo $this->l('form_add'); ?> <?php echo $subject?></div>	-->
    <?php echo form_open(base_url('registro/index/insert'), 'method="post" onsubmit="return sendform(this)" id="crudForm" autocomplete="off" role="form" enctype="multipart/form-data"'); ?>
        <?php   
            //print_r($input_fields);
            $counter = 0;
            foreach($fields as $field):                    
                $counter++;
        ?>          
        <?php if($counter<=2 && $input_fields[$field->field_name]->extras=='text_editor'): $counter=0; ?></div><?php endif ?>
        <?php if($counter<2 || $input_fields[$field->field_name]->extras=='text_editor'): ?><div class="row"><?php endif ?>        
        <div class="col-xs-12 <?= $input_fields[$field->field_name]->extras=='text_editor'?'col-sm-12':'col-sm-12' ?>" id="<?php echo $field->field_name; ?>_field_box">
            <div class="form-group">
                <!--<label id="<?php echo $field->field_name; ?>_display_as_box" for="field-<?= $field->field_name ?>">
                    <?php echo $input_fields[$field->field_name]->display_as; ?> 
                    <?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""; ?> :
                </label>-->
                <?php echo $input_fields[$field->field_name]->input?>
            </div>
        </div>        
        <?php if($counter==2 || $input_fields[$field->field_name]->extras=='text_editor'): ?></div><?php endif ?>
        <?php $counter = $counter==2?0:$counter; ?>
        <?php endforeach ?>
        <?php
            foreach($hidden_fields as $hidden_field){
                    echo $hidden_field->input;
            }
        ?>
    <?php if($counter<2): ?></div><?php endif ?>
    <div class='row'>
            <div id='report-error' class='alert alert-danger' style='display: none'></div>
            <div id='report-success' class='alert alert-success' style='display:none'></div>
            <div>
                <button id="form-button-save" type='submit'  class="btn btn-success"><?php echo $this->l('form_save'); ?></button>
                <?php 	if(!$this->unset_back_to_list) { ?>			
                <div id="save-and-go-back-button" class="btn btn-success"><?php echo $this->l('form_save_and_go_back'); ?></div>
                <button class="btn btn-danger" id="cancel-button"><?php echo $this->l('form_cancel'); ?></button>
                <?php 	} ?>
            </div>
    </div>
    <?php echo form_close(); ?>
</div>